package controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import consts.SystemConstants;
import consts.ViewConstants;
import form.ExceptionMainForm;
import utils.ApplicationContextProvider;
import utils.ControllerUtils;
import utils.SessionUtils;

public abstract class BaseController implements HandlerExceptionResolver {

	protected SessionUtils sessionUtils = new SessionUtils();

	protected final String getMessage(final String code, final Object[] params) {

		String message = null;
		Locale currentLocale = sessionUtils.getCurrentLocale();
		message = this.messageSource.getMessage(code, params, currentLocale);

		return message;
	}

	private ResourceBundleMessageSource messageSource = (ResourceBundleMessageSource) ApplicationContextProvider
			.getBean("messageSource");

	protected final ModelAndView redirectToLogout() {

		return new ModelAndView("redirect:" + ControllerUtils.LOGOUT_CONTROLLER_LOGOUT_ACTION);
	}

	/**
	 * 
	 * <p>
	 * Handle exception .
	 * </p>
	 * 
	 * @param request  HttpServletRequest
	 * @param response HttpServletResponse
	 * @param handler  Object
	 * @param ex       Exception
	 * @return ModelAndView
	 * @see (Related item)
	 */
	@Override
	public final ModelAndView resolveException(final HttpServletRequest request, final HttpServletResponse response,
			final Object handler, final Exception ex) {

		ExceptionMainForm model = new ExceptionMainForm();
		model.setException(ex);

		return new ModelAndView(ViewConstants.ERROR_VIEW, SystemConstants.DEFAULT_MODEL_MAPPING_NAME, model);
	}

}
