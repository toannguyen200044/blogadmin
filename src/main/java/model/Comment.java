package model;

import java.io.Serializable;
import java.security.Timestamp;

public class Comment implements Serializable {

	private static final long serialVersionUID = -3416687332732411829L;
	private Integer id;
	private String description;
	private Timestamp date_comment;
	private Timestamp time_comment;
	private String user_id;
	private String post_id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getDate_comment() {
		return date_comment;
	}

	public void setDate_comment(Timestamp date_comment) {
		this.date_comment = date_comment;
	}

	public Timestamp getTime_comment() {
		return time_comment;
	}

	public void setTime_comment(Timestamp time_comment) {
		this.time_comment = time_comment;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getPost_id() {
		return post_id;
	}

	public void setPost_id(String post_id) {
		this.post_id = post_id;
	}

}
