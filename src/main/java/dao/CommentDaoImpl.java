package dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Repository;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.util.JSON;

import dto.CommentDto;
import model.Comment;
import translator.CommentTranslator;
import utils.MongoUtils;

@SuppressWarnings("deprecation")
@Repository
public class CommentDaoImpl implements CommentDao {
	private ObjectMapper jsonMapper = new ObjectMapper();

	@Override
	public List<CommentDto> getListCommetByUserId(String userId) {
		MongoClient mongoClient = MongoUtils.getMongoClient();
		MongoDatabase database = mongoClient.getDatabase(MongoUtils.DB_NAME);
		MongoCollection<Document> collection = database.getCollection(MongoUtils.DB_TABLE_COMMENT);
		List<CommentDto> result = new ArrayList<>();
		Document query = new Document();
		query.append("user_id", userId);
		try (MongoCursor<Document> cursor = collection.find(query).iterator()) {
			while (cursor.hasNext()) {
				BasicDBObject obj = new BasicDBObject(cursor.next());
				obj.removeField("_id");
				Comment comment = jsonMapper.readValue(obj.toString(), Comment.class);
				result.add(CommentTranslator.toDto(comment));
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public boolean addComment(Comment obj) {
		MongoClient mongoClient = MongoUtils.getMongoClient();
		MongoDatabase database = mongoClient.getDatabase(MongoUtils.DB_NAME);
		MongoCollection<Document> collection = database.getCollection(MongoUtils.DB_TABLE_COMMENT);
		try {
			BasicDBObject dbObject = (BasicDBObject) JSON.parse(jsonMapper.writeValueAsString(obj));
			collection.insertOne(new Document(dbObject));
			return true;
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean updateComment(Comment obj) {
		MongoClient mongoClient = MongoUtils.getMongoClient();
		MongoDatabase database = mongoClient.getDatabase(MongoUtils.DB_NAME);
		MongoCollection<Document> collection = database.getCollection(MongoUtils.DB_TABLE_COMMENT);
		Document query = new Document();
		query.append("id", obj.getId());
		try {
			BasicDBObject dbObject = (BasicDBObject) JSON.parse(jsonMapper.writeValueAsString(obj));
			collection.updateOne(query, new Document().append("$set", dbObject));
			return true;
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteCommentById(Integer id) {
		MongoClient mongoClient = MongoUtils.getMongoClient();
		MongoDatabase database = mongoClient.getDatabase(MongoUtils.DB_NAME);
		MongoCollection<Document> collection = database.getCollection(MongoUtils.DB_TABLE_COMMENT);
		Document query = new Document();
		query.append("id", id);
		DeleteResult deleteResult = collection.deleteOne(query);
		return deleteResult.getDeletedCount() > 0;
	}
	
	@Override
	public Integer getMaxZoneId() {
		MongoClient mongoClient = MongoUtils.getMongoClient();
		MongoDatabase database = mongoClient.getDatabase(MongoUtils.DB_NAME);
		MongoCollection<Document> collection = database.getCollection(MongoUtils.DB_TABLE_COMMENT);
		try (MongoCursor<Document> cursor = collection.find().sort(new Document("id", -1)).limit(1).iterator()) {
			if (cursor.hasNext()) {
				BasicDBObject obj = new BasicDBObject(cursor.next());
				Integer id = obj.getInt("id");
				return id;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
