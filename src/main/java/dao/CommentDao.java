package dao;

import java.util.List;

import dto.CommentDto;
import model.Comment;

public interface CommentDao {

	List<CommentDto> getListCommetByUserId(String id);

	boolean addComment(Comment obj);

	boolean updateComment(Comment obj);

	Integer getMaxZoneId();

	boolean deleteCommentById(Integer id);

}
