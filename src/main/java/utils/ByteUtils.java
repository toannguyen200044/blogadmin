package utils;


import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Byte utility class for processing in byte units
 * 
 * @author phuongnh
 *
 */
public class ByteUtils {

	/**
	 * Create string log from byte array
	 * 
	 * @param byteArray
	 * @return
	 */
	public static String createStringFrom(byte[] byteArray, int length) {

		StringBuilder logBuilder = new StringBuilder();
		for (int i = 0; i < length; i++) {
			logBuilder.append(String.format("0x%02X ", byteArray[i]));
		}

		logBuilder.deleteCharAt(logBuilder.length() - 1);
		return logBuilder.toString();
	}

	public static String toBinaryStringFromInt16(int int16) {
		String bin = Integer.toBinaryString(0x10000 | int16).substring(1);
		if (bin.length() > 16) {
			bin = bin.substring(bin.length() - 16).trim();
		}
		return bin;
	}

	/**
	 * Convert from short array to byte array, big-endian
	 * 
	 * @return
	 */
	public static byte[] bytesFromShortBE(short... values) {
		byte[] result = new byte[values.length * 2];

		for (int i = 0; i < values.length; i++) {
			result[i * 2] = (byte) (values[i] >> 8);
			result[i * 2 + 1] = (byte) (values[i] & 0xff);
		}

		return result;
	}

	/**
	 * Convert from byte array to int 16
	 * 
	 * @param bytes
	 * @param offset
	 * @return
	 */
	public static int parseBE_UInt16(byte[] bytes, int offset) {
		return ((bytes[offset] & 0xFF) << 8) + (bytes[offset + 1] & 0xFF);
	}

	/**
	 * Convert from byte array to int 16 
	 * @param bytes
	 * @param startAddress
	 * @return
	 */
	public static int toUnsignedInt16(byte[] bytes, int startAddress) {
		int result = 0;
		for (int i = 0; i < 2; i++) {
	        int shift = (2 - 1 - i) * 8;
	        result += (bytes[i + startAddress] & 0x000000FF) << shift;
	    }
		return result;
	}

	public static long toUnsignedLong32(byte[] bytes, int startAddress) {
		long result = 0;
		for (int i = 0; i < 4; i++) {
			int shift = (4 - 1 - i) * 8;
			result += (long) (bytes[i + startAddress] & 0x000000FF) << shift;
		}
		return result;
	}

	/**
	 * Convert to relative date with input parameter
	 * 
	 * @return
	 */
	public static Date byteToRelativeDate(ByteBuffer byteBuffer) {
		int resultConvertDate = byteBuffer.getShort();
		int day = (resultConvertDate >> 11) & 0x1F;
		int month = (resultConvertDate >> 7) & 0x0F;
		int year = resultConvertDate & 0x7F;

		int resultConvertHourMinutes = byteBuffer.getShort();
		int hour = (resultConvertHourMinutes >> 8) & 0xFF;
		int minutes = resultConvertHourMinutes & 0xFF;

		int resultConvertSecondMilli = byteBuffer.getShort();
		int sec = (resultConvertSecondMilli >> 8) & 0xFF;
		int mil = resultConvertSecondMilli & 0xFF;

		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month - 1, day, hour, minutes, sec);
		calendar.set(Calendar.MILLISECOND, mil * 10);
		return calendar.getTime();
	}

	/**
	 * Convert to relative date with input parameter
	 * 
	 * @param startAddress
	 * @param startYear
	 * @return
	 */
	public static Date byteToRealDate(ByteBuffer byteBuffer, int startAddress, int startYear) {
		int resultConvertDate = byteBuffer.getShort(startAddress);
		int day = resultConvertDate >> 11;
		int month = (resultConvertDate >> 7) & 15;
		int year = (resultConvertDate) & 127;

		int resultConvertHourMinutes = byteBuffer.getShort(startAddress + 2);
		int hour = resultConvertHourMinutes >> 8;
		int minutes = resultConvertHourMinutes & 255;

		int resultConvertSecondMilli = byteBuffer.getShort(startAddress + 4);
		int sec = resultConvertSecondMilli >> 8;
		int mil = resultConvertSecondMilli & 255;

		Calendar calendar = Calendar.getInstance();
		calendar.set(year + startYear, month - 1, day, hour, minutes, sec);
		calendar.set(Calendar.MILLISECOND, mil * 10);
		return calendar.getTime();
	}

	/**
	 * convert byte array to dimming point dto
	 * 
	 * @param bytes
	 * @param startAddress
	 * @return
	 */

	/**
	 * Put dimming point into Modbus register as short buffer
	 * 
	 * @param shortBuffer     buffer to put dimming point into
	 * @param dimmingPointDto the dimming point
	 */

	public static byte[] byteFromStraightDate(Timestamp straightDate) {
		if (straightDate == null) {
			byte[] bytes = new byte[2];
			return bytes;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(straightDate.getTime());
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH) + 1;
		int year = 127; // any year
		int date = (day << 11) + (month << 7) + year;
		return bytesFromShortBE((short) date);
	}

	public static short shortFromStraightDate(Timestamp straightDate) {
		if (straightDate == null) {
			return 0;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(straightDate.getTime());
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH) + 1;
		int year = 127; // any year
		int date = (day << 11) + (month << 7) + year;
		return (short) date;
	}

	/**
	 * byte From Current DateTime
	 * 
	 * @param registersValue
	 * @param startYear      start year
	 * @return start year, current date, Current Time: Hour/Minute and Current Time:
	 *         Secs/10s of mS in byte array
	 */
	public static void putCurrentDateTime(ShortBuffer registersValue, int startYear, Calendar current) {
		// current date
		int year = current.get(Calendar.YEAR) - startYear;
		int month = current.get(Calendar.MONTH) + 1;
		int day = current.get(Calendar.DATE);

		int date = (day << 11) + (month << 7) + year;

		// Current Time: Hour/Minute
		int hour = current.get(Calendar.HOUR_OF_DAY);
		int minute = current.get(Calendar.MINUTE);

		// Current Time: Secs/10s of mS
		int second = current.get(Calendar.SECOND);
		int milisecond = current.get(Calendar.MILLISECOND) / 10;

		// get bytes
		registersValue.put((short) startYear);
		registersValue.put((short) date);
		registersValue.put((short) (((hour & 0xff) << 8) | (minute & 0xff)));
		registersValue.put((short) (((second & 0xff) << 8) | (milisecond & 0xff)));
	}

	/**
	 * Convert hour and minutes to short value.
	 * 
	 * @param hour
	 * @param minute
	 * @return
	 */
	public static short convertHourMinuteToShort(int hour, int minute) {
		short value = (short) (hour << 8 | minute);
		return value;
	}

	/**
	 * Convert unsigned 32-bit Long To Dotted Decimal
	 * 
	 * @param number unsigned 32-bit Long number
	 * @return
	 */
	public static String unsignedLong32ToDottedDecimal(long number) {
		return (short) (number >> 24 & 0xff) + "." + (short) (number >> 16 & 0xff) + "." + (short) (number >> 8 & 0xff) + "." + (short) (number & 0xff);
	}

	/**
	 * Convert unsigned 32-bit Long To Dotted Decimal
	 * 
	 * @return
	 */
	public static Long dottedDecimalToUnsignedLong32(String dottedDecimal) {
		if (dottedDecimal.isEmpty() || dottedDecimal.split("\\.").length < 4) {
			return null;
		}
		long result = 0;
		result += Long.parseLong(dottedDecimal.split("\\.")[0]) << 24;
		result += Long.parseLong(dottedDecimal.split("\\.")[1]) << 16;
		result += Long.parseLong(dottedDecimal.split("\\.")[2]) << 8;
		result += Long.parseLong(dottedDecimal.split("\\.")[3]);
		return result;
	}

	public static String convertInt32ToBinaryString2Byte(int dpsStatus) {
		String dpsStatusString = Integer.toBinaryString(dpsStatus);
		int length = dpsStatusString.length();
		String statusString;
		if (length < 16) {
			statusString = "0";
			statusString += dpsStatusString;
		} else {
			statusString = dpsStatusString;
		}
		return statusString;
	}

	public static Map<String, Long> covertDottedDecimalToMaxAndMinUnsignedLong32(String deviceId) {
		Map<String, Long> result = new HashMap<>();
		if (deviceId != null && !deviceId.isEmpty()) {
			String[] arrayDeviceId = deviceId.trim().split("\\.");

			if (arrayDeviceId.length <= 4) {
				long min = 0;
				long max = 0;
				try {
					for (String s : arrayDeviceId) {
						int value = Integer.valueOf(s);
						if (value < 0 || value > 255) {
							throw new NumberFormatException();
						}
						min = min * 256 + value;
					}
					max = min;
					for (int i = 0; i < (4 - arrayDeviceId.length); i++) {
						min = min * 256;
						max = (max * 256) + 255;
					}
					result.put("max", max);
					result.put("min", min);
				} catch (NumberFormatException e) {
					// Incorrect format, return empty search
					return null;
				}
			} else {
				// Incorrect format, return empty search
				return null;
			}
		}
		return result;
	}

	public static short convertTwoBytesToShort(byte[] bytes) {
		int value = bytes[1] | bytes[0] << 8;
		return (short) value;
	}

	public static String[] convertToHexStringArray(byte[] byteArray) {
		int length = byteArray.length;
		String[] result = new String[length];
		for (int i = 0; i < length; i++) {
			result[i] = String.format("%02X", byteArray[i]);
		}
		return result;
	}
}

