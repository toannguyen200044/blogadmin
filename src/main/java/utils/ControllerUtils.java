package utils;

public class ControllerUtils {
	
	public static final String USER_CONTROLLER_MAIN_ACTION = "/user";
	public static final String USER_CONTROLLER_ADD_ACTION = "/user/Add";
	public static final String USER_CONTROLLER_SEARCH_LIST = "/user/search";
	public static final String USER_CONTROLLER_DELETE_ACTION = "/user/delete";
	public static final String USER_CONTROLLER_UPDATE_ACTION = "/user/update";
	
	public static final String LOGOUT_CONTROLLER_LOGOUT_ACTION = "/logout";
	
}
