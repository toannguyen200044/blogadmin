package utils;

public class ControllerUtilApi {
	
	public static final String COMMENT_API_LIST = "/api/comment/list";
	public static final String COMMENT_API_DETAIL = "/api/comment";
	
	public static final String FOOD_API_LIST = "/api/food/list";

}
