package utils;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;

public class SessionUtils {

	@SuppressWarnings("unused")
    private static Logger logger = LoggerFactory.getLogger(SessionUtils.class);
	
	public static final String COMMUNICATION_SESSION_ATTACHED_FILE_LIST = "CommunicationFax_AttachedFileList";
	
	public SessionUtils() {
        
    }
	
	
    public final Locale getCurrentLocale() {
        
        Locale locale = LocaleContextHolder.getLocale();
        return locale;
    }
    
    
}
