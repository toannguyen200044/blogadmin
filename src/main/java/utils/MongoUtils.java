package utils;

import com.mongodb.MongoClient;

public class MongoUtils {

	private static final String HOST = "localhost";
	private static final int PORT = 27017;
	public static final String DB_NAME = "blogAdmin";
	public static final String DB_TABLE_COMMENT = "Comment";


	private static MongoClient mongoClient = null;

	public static MongoClient getMongoClient() {
		if (mongoClient == null) {
			mongoClient = new MongoClient(HOST, PORT) ;
			return mongoClient;
		} else {
			return mongoClient;
		}
	}

}
