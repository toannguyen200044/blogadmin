package utils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class ApplicationContextProvider implements ApplicationContextAware {

	private static ApplicationContext context = null;
    /*
     * (non-Javadoc)
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext(
     * org.springframework.context.ApplicationContext)
     */
    @Override
    public final void setApplicationContext(final ApplicationContext context) {
        ApplicationContextProvider.context = context;
    }

    /**
     * 
     * <p>
     * Get bean from config.
     * </p>
     * @param beanId String
     * @return Object
     */
    public static Object getBean(final String beanId) {        
        return context.getBean(beanId);
    }
    /**
     * <p>
     * Get a bean object.
     * </p>
     * @param cl {@link Class}
     * @return T
     * @param <T> T
     */
    public static <T> T getBean(final Class<T> cl) {
        return context.getBean(cl);
    }
}
