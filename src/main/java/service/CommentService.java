package service;

import java.util.List;

import dto.CommentDto;

public interface CommentService {

	List<CommentDto> getListCommentByUserId(String userId);

	CommentDto createComment(CommentDto commentDto);

	boolean updateComment(CommentDto zoneDto);

	boolean deleteCommentByIds(String ids);

}
