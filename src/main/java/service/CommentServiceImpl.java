package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import dao.CommentDao;
import dto.CommentDto;
import model.Comment;
import translator.CommentTranslator;

public class CommentServiceImpl implements CommentService {

	@Autowired
	private CommentDao commentRepository;

	@Override
	public List<CommentDto> getListCommentByUserId(String userId) {
		return commentRepository.getListCommetByUserId(userId);
	}

	@Override
	public boolean deleteCommentByIds(String ids) {
		Boolean result = false;
		String[] lstIds = ids.split(",");
		for (String i : lstIds) {
			Integer id = Integer.valueOf(i);
			if (commentRepository.deleteCommentById(id)) {
				result = true;
			}
		}
		return result;
	}

	@Override
	public CommentDto createComment(CommentDto commentDto) {
		Integer id = commentRepository.getMaxZoneId();
		id = id == null ? 1 : id + 1;
		commentDto.setId(id);
		Comment commentEntity = CommentTranslator.toEntity(commentDto);
		if (commentRepository.addComment(commentEntity)) {
			return commentDto;
		}
		return null;
	}

	@Override
	public boolean updateComment(CommentDto zoneDto) {
		Comment entity = CommentTranslator.toEntity(zoneDto);
		return commentRepository.updateComment(entity);
	}

}
