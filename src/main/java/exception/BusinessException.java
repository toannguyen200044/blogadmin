package exception;

public class BusinessException extends RuntimeException {

private static final long serialVersionUID = 2388796371935040758L;
    
    /**
     * Constructor.
     * @param ex CofPersistenceException
     */
    public BusinessException(final BusinessException ex) {
        super(ex.getMessage());
        setErrorCode(ex.getErrorCode());
    }
    
    /**
     * The error code.
     * @see {@link ErrorCodeConstant}
     */
    private String errorCode;
    
    /***
     * Params for error message
     */
    private Object[] params;
        
    /**
	 * @return the params.
	 */
	public final Object[] getParams() {
		return params;
	}
	/**
	 * @param params the params to set.
	 */
	public final void setParams(Object[] params) {
		this.params = params;
	}
	/**
     * Constructor.
     * 
     * @param errorCode String error Code
     * @see {@link ErrorCodeConstant}
     */
    public BusinessException(final String errorCode) {
        this.errorCode = errorCode;
    }
    /**
     * Get the errorCode attribute.
     * @return the errorCode
     */
    public final String getErrorCode() {
        return errorCode;
    }
    /**
     * Set the errorCode attribute.
     * @param errorCode the errorCode to set
     */
    public final void setErrorCode(final String errorCode) {
        this.errorCode = errorCode;
    }
    
    /**
     * Constructor.
     * 
     * @param errorCode String error Code
     * @param errorMessage String error Message
     *
     */
    public BusinessException(final String errorCode, final String errorMessage) {
        super(errorMessage);
        this.errorCode = errorCode;
    }
}
