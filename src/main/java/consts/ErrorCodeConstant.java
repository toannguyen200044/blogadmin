package consts;

public class ErrorCodeConstant {
	
	public static final String INTERNAL_ERROR = "error.internal";

	/**
	 * Access denied.
	 */
	public static final String ACCESS_DENIED = "access.denied";

	/**
	 * Service is not available. It may be deleted, inactive or occupied.
	 */
	public static final String SERVICE_NOT_AVAILABLE = "service.not_available";
	
	public static final String USER_EXISTED = "error.user.existed";
	public static final String EMAIL_EXISTED = "error.user.email.existed";
	
	public static final String DUPLICATE_ID_ERROR_USER = "error.duplicate.id.user";
	public static final String DUPLICATE_EMAIL_ERROR_USER = "error.duplicate.email.user";
	public static final String DUPLICATE_USERNAME_ERROR_USER = "error.duplicate.username.user";
	
	public static final String ADDRESS_EXISTED = "error.address.existed";
	

}
