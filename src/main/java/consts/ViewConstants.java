package consts;

public class ViewConstants {
	
	public static final String USER_VIEW = "user";
	
	public static final String FOOD_VIEW = "food";
	
	public static final String RESTAURANT_VIEW = "restaurant";
	
	public static final String EVENT_VIEW = "eventlog";
	
	public static final String ERROR_VIEW = "error";

}
