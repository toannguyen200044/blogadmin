package consts;

public final class SystemConstants {
    
    /**
     * Private constructor for utility class.
     */
    private SystemConstants() {
        
    }
	
    /**
     * Maximum records per pag display default. 
     */
    public static final int DEFAULT_MAXIMUM_PAGING_DISPLAY = 15;
    
    /**
     * Default model mapping name.
     */
    public static final String DEFAULT_MODEL_MAPPING_NAME = "model";
    
    /**
     * Select all option value.
     */
    public static final String SELECT_ALL_OPTION_VALUE = "All";
    
    /**
     * Select none option value.
     */
    public static final String SELECT_ALL_OPTION_NONE = "None";
    
    /**
     * Select Yes option value.
     */
    public static final String YES_OPTION_VALUE = "true";
    
    /**
     * Select No option value.
     */
    public static final String NO_OPTION_VALUE = "false";
    
    /**
     * Upload oversize exception key.
     */
    public static final String UPLOAD_OVERSIZE_EXCEPTION_KEY = "uploadoversize";
    
    /**
     * Full calendar hyphen splitter values.
     */
    public static final String FULL_CALENDAR_HYPHEN_SPLITTER = ",";
}
