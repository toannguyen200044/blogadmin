package form;

public class BasePagingResponseForm extends BaseResponseForm {

	private static final long serialVersionUID = -7535723063907460846L;

	private int indexRequest;

	/**
	 * Number of records return in this result.
	 */
	private int returnRecords;

	/**
	 * Number of total records for the query.
	 */
	private long totalRecords;

	/**
	 * Get the totalRecords attribute.
	 * 
	 * @return the totalRecords
	 */
	public final long getTotalRecords() {
		return totalRecords;
	}

	/**
	 * Set the totalRecords attribute.
	 * 
	 * @param totalRecords the totalRecords to set
	 */
	public final void setTotalRecords(final long totalRecords) {
		this.totalRecords = totalRecords;
	}

	/**
	 * Get the indexRequest attribute.
	 * 
	 * @return the indexRequest
	 */
	public final int getIndexRequest() {
		return indexRequest;
	}

	/**
	 * Set the indexRequest attribute.
	 * 
	 * @param indexRequest the indexRequest to set
	 */
	public final void setIndexRequest(final int indexRequest) {
		this.indexRequest = indexRequest;
	}

	/**
	 * Get the returnRecords attribute.
	 * 
	 * @return the returnRecords
	 */
	public final int getReturnRecords() {
		return returnRecords;
	}

	/**
	 * Set the returnRecords attribute.
	 * 
	 * @param returnRecords the returnRecords to set
	 */
	public final void setReturnRecords(final int returnRecords) {
		this.returnRecords = returnRecords;
	}

}
