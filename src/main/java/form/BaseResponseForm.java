package form;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BaseResponseForm implements Serializable {

	private static final long serialVersionUID = -7535723063907460846L;


    /**
     * Is data return valid.
     */
    private boolean validData = true;
    
    /**
     * List of error code in case of data is not valid.
     */
    private List<String> listErrorCode = new ArrayList<>();
    
    /**
     * List of error message in case of data is not valid.
     */
    private List<String> listErrorMessage = new ArrayList<>();

    /**
     * Get the validData attribute.
     * @return the validData
     */
    public final boolean getValidData() {
        return validData;
    }

    /**
     * Set the validData attribute.
     * @param validData the validData to set
     */
    public final void setValidData(final boolean validData) {
        this.validData = validData;
    }

    /**
     * Get the listErrorCode attribute.
     * @return the listErrorCode
     */
    public final List<String> getListErrorCode() {
        return listErrorCode;
    }

    /**
     * Set the listErrorCode attribute.
     * @param listErrorCode the listErrorCode to set
     */
    public final void setListErrorCode(final List<String> listErrorCode) {
        this.listErrorCode = listErrorCode;
    }

    /**
     * Get the listErrorMessage attribute.
     * @return the listErrorMessage
     */
    public final List<String> getListErrorMessage() {
        return listErrorMessage;
    }

    /**
     * Set the listErrorMessage attribute.
     * @param listErrorMessage the listErrorMessage to set
     */
    public final void setListErrorMessage(final List<String> listErrorMessage) {
        this.listErrorMessage = listErrorMessage;
    }
}
