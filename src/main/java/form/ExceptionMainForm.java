package form;

public class ExceptionMainForm {
	
	private Exception exception;

	public final Exception getException() {
        return exception;
    }

    /**
     * Set the exception attribute.
     * @param exception the exception to set
     */
    public final void setException(final Exception exception) {
        this.exception = exception;
    }
}
