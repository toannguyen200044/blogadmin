package translator;

import dto.CommentDto;
import model.Comment;

public class CommentTranslator {
	public static Comment toEntity(CommentDto commentDto) {
		Comment comment = new Comment();
		if (commentDto.getId() != null) {
			comment.setId(commentDto.getId());
		}
		comment.setDate_comment(commentDto.getDate_comment());
		comment.setDescription(commentDto.getDescription());
		comment.setTime_comment(commentDto.getTime_comment());
		comment.setPost_id(commentDto.getPost_id());
		comment.setUser_id(commentDto.getUser_id());
		return comment;
	}

	public static CommentDto toDto(Comment comment){
		CommentDto commentDto = new CommentDto();
		if(comment!=null) {
			if (comment.getId() != null) {
				commentDto.setId(comment.getId());
			}
			commentDto.setDate_comment(comment.getDate_comment());
			commentDto.setDescription(comment.getDescription());
			commentDto.setTime_comment(comment.getTime_comment());
			commentDto.setPost_id(comment.getPost_id());
			commentDto.setUser_id(comment.getUser_id());
		}
		return commentDto;
	}

}
