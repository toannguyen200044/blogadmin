<html lang="en">
 <head>
   <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/jquery-ui/jquery-ui-1.8.20.custom.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/style.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/header-card.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/vendors/pace-progress/css/pace.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/bootstrap.min.css">
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      // Shared ID
      gtag('config', 'UA-118965717-3');
      // Bootstrap ID
      gtag('config', 'UA-118965717-5');
    </script>
  </head>
  <body class="app flex-row align-items-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-6">
          <div class="card mx-4">
            <div class="card-body p-4">
              <h1>Register</h1>
              <p class="text-muted">Create your account</p>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="icon-user"></i>
                  </span>
                </div>
                <input class="form-control" type="text" placeholder="Username">
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">@</span>
                </div>
                <input class="form-control" type="text" placeholder="Email">
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="icon-lock"></i>
                  </span>
                </div>
                <input class="form-control" type="password" placeholder="Password">
              </div>
              <div class="input-group mb-4">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="icon-lock"></i>
                  </span>
                </div>
                <input class="form-control" type="password" placeholder="Repeat password">
              </div>
              <button class="btn btn-block btn-success" type="button">Create Account</button>
            </div>         
          </div>
        </div>
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
  </body>
</html>