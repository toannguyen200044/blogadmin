<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Student Scores</title>
	<style>
		tr:first-child{
			font-weight: bold;
			background-color: #C6C9C4;
		}
	</style>
<script type="text/javascript">
function myFunction() {
   
    if (confirm("Press a button!")==true) {
       return true;
    }  
    else
        return false;
}
</script>
</head>
<body>
<h2>Student Information</h2>	
	<ul>
  	<li>Street : ${address.street}</li>
  	<li>City : ${address.city}</li>
  	<li>Country : ${address.country}</li>
	</ul>
<h2>List of Scores</h2>	
	<table>
		<tr>
			<td>Subject</td>
			<td>Score</td>			
			<td></td>
			<td></td>
		</tr>
		<c:forEach items="${scores}" var="scores">
			<tr>
			<td>${scores.subject}</td>
			<td>${scores.score}</td>			
			<td><a href="<c:url value='/edit-${scores.scoreId}-scores' />">edit</a></td>	
			<td><a onclick=" return myFunction()"  href="<c:url value='/delete-${scores.scoreId}-scores'/>">delete</a></td>		
			</tr>
		</c:forEach>		
	</table>
	<a href="<c:url value='/new' />">Add New Score</a>
	<br/>
	<br/>
	Average Score : ${avgscore}
</body>
</html>