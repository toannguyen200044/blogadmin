<html lang="en">
 <head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<title>Event Log</title>
<% long timestamp = new java.util.Date().getTime(); %>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/all.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" />

<!-- Main styles for this application-->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/jquery-ui/jquery-ui-1.8.20.custom.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/style.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/header-card.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/vendors/pace-progress/css/pace.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/bootstrap.min.css">
<script type="text/javascript" src="${controllerUtils.dynamicConfigAction}"></script>
<script type="text/javascript" src="${controllerUtils.generatorUrlAjaxAction}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/jquery.ui/jquery-ui-1.8.20.custom.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="//maps.google.com/maps/api/js?key=AIzaSyCnBxsCyBZImkEf-mjyNIPA0fo4aER2IOw"></script>
<script src="//maps.google.com/maps"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/pace.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/header-card.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/momentjs/moment.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/jquery.json-2.3.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/header-card.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.2/underscore-min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/custom-tooltips.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/sweetalert/sweetalert.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/script/common/sweetalert/sweetalert.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/app.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/datatables/dataTables.bootstrap4.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/ui-bootstrap-tpls-2.5.0.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/eventlog/eventlog.service.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/eventlog/eventlog.controller.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/baseui.min.js"></script>
</head>
<body>
<div id="page-content" class="container-fluid" ng-app="myApp" ng-controller="eventLogController" style="padding-left: 0px; padding-right: 0px;">
	<div id="ui-view">
		<div class="animated fadeIn">
			<div class="card">
				<div class="card-header">
					<div class="dataTables_wrapper dt-bootstrap4 no-footer">
						<div class="row align-items-center">
							<div class="col-sm-12 col-md-4">
							</div>
							<div class="col-sm-12 col-md-5">
								<div class="row align-items-center">
									<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
									</div>
									<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
										<button 
											class="btn btn-success btn-sm" type="button" data-toggle="modal"
											data-target="#addRestaurantModal"> 
											<i class="fas fa-plus"></i>
											<spring:message code="frontend.view.button.add" />
										</button>
									</div>
									<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
										<button 
											class="btn btn-danger btn-sm" type="button" 
											ng-click="deleteAllRestaurant()">
											<i class="fas fa-times"></i>
											<spring:message code="frontend.view.button.delete" />
										</button>
									</div>
									<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
										<a href="/final-excercise/admin"><button 
											class="btn btn-info btn-sm" type="button">
											<i class="fa fa-home"></i>
											<spring:message code="frontend.view.button.home" />
										</button> </a>
									</div>
							</div>
							<div class="col-sm-12 col-md-3">
								<div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
									<ul class="pagination" uib-pagination total-items="total_items" ng-model="current_page" max-size="max_size" boundary-links="true" force-ellipses="true" ng-change="pageChanged()" items-per-page="page_size" first-text="{{paginationFirstText}}" previous-text="{{paginationPreviousText}}" next-text="{{paginationNextText}}" last-text="{{paginationLastText}}">
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-body" style = "max-height: 84vh; overflow: auto;">
					<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
						<div class="row">
							<div id="eventList" class="col-sm-12">
								<table class="table table-striped  table-responsive-sm table-bordered datatable dataTable no-footer table-hover" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
									<thead>
										<tr role="row">
											<th style="width: 7%;">
												<div class="pretty p-image p-plain">
													<input type="checkbox" ng-model="selectedAll" ng-change="selectAll()" />
													<div class="state">
														<img class="image" src="${pageContext.request.contextPath}/resources/imgs/004.png"><label></label>
													</div>
												</div>
											</th>
											<th aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 15%;"><spring:message code="frontend.view.restaurant.table.userId" /></th>
											<th aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 15%;"><spring:message code="frontend.view.restaurant.table.food" /></th>
											<th aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 15%;"><spring:message code="frontend.view.restaurant.table.restaurant" /></th>
											<th aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 20%;"><spring:message code="frontend.view.restaurant.table.time" /></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="(key,value) in data">
											<td ng-cloak>
												<div class="pretty p-image p-plain">
													<input type="checkbox" name="checkedUser" value="{{value.id}}" ng-model='toDelete[value.id]' ng-click="checkIfSelectAll()" />
													<div class="state">
														<img class="image" src="${pageContext.request.contextPath}/resources/imgs/004.png"><label></label>
													</div>
												</div>
											</td>
											<td ng-cloak>{{value.userId}}</td>
											<td ng-cloak>{{value.food}}</td>
											<td ng-cloak>{{value.restaurant}</td>
											<td ng-cloak>{{value.time}}</td>
											<td ng-cloak><a ng-click="deleteOneEvent(value.id)" <c:if test="${!isAuthorized}">disabled</c:if> class="btn btn-danger deleteLink" data-value="{{ value.id }}" href="javascript:void(0);"><span class="fas fa-trash-alt" style="font-size:128.7%"></span></a></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</body>