function STATUS_ICON_URL() {

}
STATUS_ICON_URL.groupIconUrl 					= '${pageContext.request.contextPath}/resources/imgs/common/layout/PlaceIcon_Group_small.png';
STATUS_ICON_URL.statusIconUnknowStateUrl 		= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_UNKNOW_small.png';

STATUS_ICON_URL.statusIconOffUrl 				= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_OFF_small.png';
STATUS_ICON_URL.statusIconDim20Url 				= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_20_small.png';
STATUS_ICON_URL.statusIconDim40Url 				= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_40_small.png';
STATUS_ICON_URL.statusIconDim60Url 				= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_60_small.png';
STATUS_ICON_URL.statusIconDim80Url 				= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_80_small.png';
STATUS_ICON_URL.statusIconOnUrl 				= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_ON_small.png';
STATUS_ICON_URL.statusIconDimming				= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_Dimming_small.png';
STATUS_ICON_URL.statusIconRegisterUrl			= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_REGISTRATION_small.png';

STATUS_ICON_URL.statusIconWarningOffUrl 		= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_OFF_WARNING_small.png';
STATUS_ICON_URL.statusIconWarning20Url 			= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_20_WARNING_small.png';
STATUS_ICON_URL.statusIconWarning40Url 			= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_40_WARNING_small.png';
STATUS_ICON_URL.statusIconWarning60Url 			= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_60_WARNING_small.png';
STATUS_ICON_URL.statusIconWarning80Url 			= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_80_WARNING_small.png';
STATUS_ICON_URL.statusIconWarningOnUrl 			= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_ON_WARNING_small.png';
STATUS_ICON_URL.statusIconWarningDimmingUrl 	= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_Dimming_WARNING_small.png';

STATUS_ICON_URL.statusIconErrorUrl 				= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_ERROR_small.png';
STATUS_ICON_URL.statusIconOfflineUrl 			= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_OFFLINE_small.png';
STATUS_ICON_URL.statusMlcIconOfflineUrl			= '${pageContext.request.contextPath}/resources/imgs/mlc/Mlc_Icon_Offline.png';
STATUS_ICON_URL.statusMlcIconOnlineUrl			= '${pageContext.request.contextPath}/resources/imgs/mlc/Mlc_Icon_Online.png';
STATUS_ICON_URL.closeIconUrl 					= '${pageContext.request.contextPath}/resources/imgs/common/button/close.png';

STATUS_ICON_URL.statusControlDimmingIconOffUrl 				= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_OFF.png';
STATUS_ICON_URL.statusControlDimmingIconOnUrl 				= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_ON.png';
STATUS_ICON_URL.statusControlDimmingIconDimming				= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_Dimming.png';
STATUS_ICON_URL.statusControlDimmingIconWarningOffUrl 				= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_OFF_WARNING.png';
STATUS_ICON_URL.statusControlDimmingIconWarningOnUrl 				= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_ON_WARNING.png';
STATUS_ICON_URL.statusControlDimmingIconWarningDimmingUrl				= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_Dimming_WARNING.png';
STATUS_ICON_URL.statusControlDimmingIconErrorUrl				= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_ERROR.png';
STATUS_ICON_URL.statusControlDimmingIconOfflineUrl 				= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_OFFLINE.png';
STATUS_ICON_URL.statusControlRegistrationUrl 					= '${pageContext.request.contextPath}/resources/imgs/status_icon/StatusIcon_REGISTRATION.png';

function STATUS_ICON_MULTY_SELECTED_URL() {

}
STATUS_ICON_MULTY_SELECTED_URL.statusIconUnknowStateUrl 		= '${pageContext.request.contextPath}/resources/imgs/status_icon_multy_selected/StatusIcon_UNKNOW_small.png';

STATUS_ICON_MULTY_SELECTED_URL.statusIconOffUrl 				= '${pageContext.request.contextPath}/resources/imgs/status_icon_multy_selected/StatusIcon_OFF_small.png';
STATUS_ICON_MULTY_SELECTED_URL.statusIconDim20Url 				= '${pageContext.request.contextPath}/resources/imgs/status_icon_multy_selected/StatusIcon_20_small.png';
STATUS_ICON_MULTY_SELECTED_URL.statusIconDim40Url 				= '${pageContext.request.contextPath}/resources/imgs/status_icon_multy_selected/StatusIcon_40_small.png';
STATUS_ICON_MULTY_SELECTED_URL.statusIconDim60Url 				= '${pageContext.request.contextPath}/resources/imgs/status_icon_multy_selected/StatusIcon_60_small.png';
STATUS_ICON_MULTY_SELECTED_URL.statusIconDim80Url 				= '${pageContext.request.contextPath}/resources/imgs/status_icon_multy_selected/StatusIcon_80_small.png';
STATUS_ICON_MULTY_SELECTED_URL.statusIconOnUrl 				= '${pageContext.request.contextPath}/resources/imgs/status_icon_multy_selected/StatusIcon_ON_small.png';

STATUS_ICON_MULTY_SELECTED_URL.statusIconWarningOffUrl 		= '${pageContext.request.contextPath}/resources/imgs/status_icon_multy_selected/StatusIcon_OFF_WARNING_small.png';
STATUS_ICON_MULTY_SELECTED_URL.statusIconWarning20Url 			= '${pageContext.request.contextPath}/resources/imgs/status_icon_multy_selected/StatusIcon_20_WARNING_small.png';
STATUS_ICON_MULTY_SELECTED_URL.statusIconWarning40Url 			= '${pageContext.request.contextPath}/resources/imgs/status_icon_multy_selected/StatusIcon_40_WARNING_small.png';
STATUS_ICON_MULTY_SELECTED_URL.statusIconWarning60Url 			= '${pageContext.request.contextPath}/resources/imgs/status_icon_multy_selected/StatusIcon_60_WARNING_small.png';
STATUS_ICON_MULTY_SELECTED_URL.statusIconWarning80Url 			= '${pageContext.request.contextPath}/resources/imgs/status_icon_multy_selected/StatusIcon_80_WARNING_small.png';
STATUS_ICON_MULTY_SELECTED_URL.statusIconWarningOnUrl 				= '${pageContext.request.contextPath}/resources/imgs/status_icon_multy_selected/StatusIcon_ON_WARNING_small.png';

STATUS_ICON_MULTY_SELECTED_URL.statusIconErrorUrl 				= '${pageContext.request.contextPath}/resources/imgs/status_icon_multy_selected/StatusIcon_ERROR_small.png';
STATUS_ICON_MULTY_SELECTED_URL.statusIconOfflineUrl 			= '${pageContext.request.contextPath}/resources/imgs/status_icon_multy_selected/StatusIcon_OFFLINE_small.png';
