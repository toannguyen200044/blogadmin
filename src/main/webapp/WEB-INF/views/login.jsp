<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login Page</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/jquery-ui/jquery-ui-1.8.20.custom.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/style.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/header-card.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/vendors/pace-progress/css/pace.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/bootstrap.min.css">
</head>
<body>
	<body class="app flex-row align-items-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                <h1>Login</h1>
                <p class="text-muted">Sign In to your account</p>
                <c:if test="${error}">
					<!--  start message-red -->

					<div id="message-red">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td class="error">Wrong username or password !</td>
							</tr>
						</table>
					</div>
					<div class="clear"></div>
					<!--  end message-red -->
				</c:if>
                <form:form action="login.do" method="post"
					modelAttribute="loginAttribute">
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<th>Username</th>
							<td><form:input type="text" class="login-inp"
									path="username" /></td>
						</tr>
						<tr>
							<th>Password</th>
							<td><form:input type="text" value=""
									onfocus="this.value=''" class="login-inp" path="password" /></td>
						</tr>
						<tr>
							<th></th>
							<td valign="top"><input type="checkbox"
								class="checkbox-size" id="login-check" /><label
								for="login-check">Remember me</label></td>
						</tr>
						<tr>
							<th></th>
							<td><input type="submit" value="Login" /></td>
						</tr>
					</table>
				</form:form>
              </div>
            </div>
            <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
              <div class="card-body text-center">
                <div>
                  <h2>Sign up</h2>
                  <a href="/final-excercise/register" class="btn btn-info" role="button">Register</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</body>
</html>