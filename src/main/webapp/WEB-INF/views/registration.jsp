<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Student Score Registration Form</title>

<style>

	.error {
		color: #ff0000;
	}
</style>
<script type="text/javascript">
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode == 110) {
        return false;
    }
    return true;
}
</script>
</head>

<body>

	<h2>Registration Score Form</h2>
	<form:form method="POST" modelAttribute="score">
		<form:input type="hidden" path="scoreId" id="scoreId"/>
		<table>
			<tr>
				<td><label for="subject">Subject: </label> </td>
				<td><form:input  path="subject" id="subject"/></td>
				<td><form:errors path="subject" cssClass="error"/></td>	
				<td>
					<c:if test="${errors}">										
						<div id = "display_error" class="error" >Subject's name must be unique !</div>																	
					</c:if>
				</td>		
		    </tr>	    
			<tr>
				<td><label for="score">Score: </label> </td>
				<td><form:input path="score" id="score" onkeypress="return isNumber(event)"/></td>
				<td><form:errors path="score" cssClass="error"/></td>
		    </tr>
			<tr>
				<td colspan="3">
					<c:choose>
						<c:when test="${edit}">
							<input type="submit" value="Update"/>
						</c:when>
						<c:otherwise>
							<input type="submit" value="Add"/>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</table>
	</form:form>
	<br/>
	<br/>
	Go back to <a href="<c:url value='/allscores' />">List of All Students</a>
</body>
</html>