<!DOCTYPE html>
<html lang="en">
<head>
<base href="./">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<sec:authentication var="principal" property="principal"></sec:authentication>

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/all.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" />

<!-- Main styles for this application-->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/style/common/jquery-ui/jquery-ui-1.8.20.custom.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/style/common/style.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/style/common/header-card.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/style/vendors/pace-progress/css/pace.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/style/common/bootstrap.min.css">
<script type="text/javascript"
	src="${controllerUtils.dynamicConfigAction}"></script>
<script type="text/javascript"
	src="${controllerUtils.generatorUrlAjaxAction}"></script>

<!-- CoreUI and necessary plugins-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/script/common/jquery.ui/jquery-ui-1.8.20.custom.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/script/common/pace.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/script/common/header-card.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/script/common/momentjs/moment.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/script/common/jquery.json-2.3.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/script/common/header-card.js"></script>
<script type="text/javascript"
	src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.2/underscore-min.js"></script>
<!-- Plugins and scripts required by this view-->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/script/common/custom-tooltips.min.js"></script>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/script/common/jquery.blockUI.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/script/common/sweetalert/sweetalert.min.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/script/common/sweetalert/sweetalert.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/style/common/pretty-checkbox.min.css">

<script>
	window.dataLayer = window.dataLayer || [];
	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());
	// Shared ID
	gtag('config', 'UA-118965717-3');
	// Bootstrap ID
	gtag('config', 'UA-118965717-5');
</script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/script/common/baseui.min.js"></script>

</head>
<body
	class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<header class="app-header navbar">
		<button class="navbar-toggler sidebar-toggler d-lg-none mr-auto"
			type="button" data-toggle="sidebar-show">
			<span class="navbar-toggler-icon"></span>
		</button>
			<button class="navbar-toggler sidebar-toggler d-md-down-none"
				type="button" data-toggle="sidebar-lg-show">
				<span class="navbar-toggler-icon"></span>
			</button>
	</header>
	<div class="app-body">
		<div class="sidebar">
			<nav class="sidebar-nav">
				<ul class="nav">
					<li class="nav-title">Management</li>
					<li class="nav-item nav-dropdown"><a
						class="nav-link nav-dropdown-toggle" href="#"> <i
							class="nav-icon icon-puzzle"></i> User
					</a>
						<ul class="nav-dropdown-items">
							<li class="nav-item"><a class="nav-link"
								href="/final-excercise/user"> <i class="nav-icon icon-puzzle"></i>
									User List
							</a></li>							
						</ul></li>
					<li class="nav-item nav-dropdown"><a
						class="nav-link nav-dropdown-toggle" href="#"> <i
							class="nav-icon icon-cursor"></i> Food
					</a>
						<ul class="nav-dropdown-items">
							<li class="nav-item"><a class="nav-link"
								href="/final-excercise/food"> <i class="nav-icon icon-cursor"></i>
									Food List
							</a></li>			
						</ul></li>					
					<li class="nav-item nav-dropdown"><a
						class="nav-link nav-dropdown-toggle" href="#"> <i
							class="nav-icon icon-star"></i> Restaurant
					</a>
						<ul class="nav-dropdown-items">
							<li class="nav-item"><a class="nav-link"
								href="/final-excercise/restaurant"> <i
									class="nav-icon icon-star"></i>Restaurant List
							</a></li>						
						</ul></li>
					<li class="nav-item nav-dropdown"><a
						class="nav-link nav-dropdown-toggle" href="#"> <i
							class="nav-icon icon-cursor"></i> Event Log
					</a>
						<ul class="nav-dropdown-items">
							<li class="nav-item"><a class="nav-link"
								href="/final-excercise/event"> <i class="nav-icon icon-cursor"></i>
									List
							</a></li>			
						</ul></li>						
					<li class="divider"></li>
					<li class="nav-title">Extras</li>
					<li class="nav-item nav-dropdown"><a
						class="nav-link nav-dropdown-toggle" href="#"> <i
							class="nav-icon icon-star"></i> Pages
					</a>
						<ul class="nav-dropdown-items">
							<li class="nav-item"><a class="nav-link" href="login.html"
								target="_top"> <i class="nav-icon icon-star"></i> Login
							</a></li>
							<li class="nav-item"><a class="nav-link"
								href="register.html" target="_top"> <i
									class="nav-icon icon-star"></i> Register
							</a></li>
						</ul></li>
				</ul>
			</nav>
			<button class="sidebar-minimizer brand-minimizer" type="button"></button>
		</div>
		<main class="main"> <!-- Breadcrumb-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">Home</li>
			<li class="breadcrumb-item"><a href="#">Admin</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
			<!-- Breadcrumb Menu-->
			<li class="breadcrumb-menu d-md-down-none">
				<div class="btn-group" role="group" aria-label="Button group">
					<a class="btn" href="#"> <i class="icon-speech"></i>
					</a> <a class="btn" href="./"> <i class="icon-graph"></i>
						 Dashboard
					</a> <a class="btn" href="#"> <i class="icon-settings"></i>
						 Settings
					</a>
				</div>
			</li>
		</ol>
		<div class="container-fluid">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-sm-6 col-lg-3">
						<div class="card text-white bg-primary">
							<div class="card-body pb-0">
								<div class="btn-group float-right">
									<button class="btn btn-transparent dropdown-toggle p-0"
										type="button" data-toggle="dropdown" aria-haspopup="true"
										aria-expanded="false">
										<i class="icon-settings"></i>
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="#">Action</a> <a
											class="dropdown-item" href="#">Another action</a> <a
											class="dropdown-item" href="#">Something else here</a>
									</div>
								</div>
								<div class="text-value">9.823</div>
								<div>Members online</div>
							</div>
							<div class="chart-wrapper mt-3 mx-3" style="height: 70px;">
								<canvas class="chart" id="card-chart1" height="70"></canvas>
							</div>
						</div>
					</div>
					<!-- /.col-->
					<div class="col-sm-6 col-lg-3">
						<div class="card text-white bg-info">
							<div class="card-body pb-0">
								<div class="btn-group float-right">
									<button class="btn btn-transparent dropdown-toggle p-0"
										type="button" data-toggle="dropdown" aria-haspopup="true"
										aria-expanded="false">
										<i class="icon-settings"></i>
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="#">Action</a> <a
											class="dropdown-item" href="#">Another action</a> <a
											class="dropdown-item" href="#">Something else here</a>
									</div>
								</div>
								<div class="text-value">9.823</div>
								<div>Food Number</div>
							</div>
							<div class="chart-wrapper mt-3 mx-3" style="height: 70px;">
								<canvas class="chart" id="card-chart2" height="70"></canvas>
							</div>
						</div>
					</div>
					<!-- /.col-->
					<div class="col-sm-6 col-lg-3">
						<div class="card text-white bg-warning">
							<div class="card-body pb-0">
								<div class="btn-group float-right">
									<button class="btn btn-transparent dropdown-toggle p-0"
										type="button" data-toggle="dropdown" aria-haspopup="true"
										aria-expanded="false">
										<i class="icon-settings"></i>
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="#">Action</a> <a
											class="dropdown-item" href="#">Another action</a> <a
											class="dropdown-item" href="#">Something else here</a>
									</div>
								</div>
								<div class="text-value">9.823</div>
								<div>Restaurant Number</div>
							</div>
							<div class="chart-wrapper mt-3" style="height: 70px;">
								<canvas class="chart" id="card-chart3" height="70"></canvas>
							</div>
						</div>
					</div>
					<!-- /.col-->
					<div class="col-sm-6 col-lg-3">
						<div class="card text-white bg-danger">
							<div class="card-body pb-0">
								<div class="btn-group float-right">
									<button class="btn btn-transparent dropdown-toggle p-0"
										type="button" data-toggle="dropdown" aria-haspopup="true"
										aria-expanded="false">
										<i class="icon-settings"></i>
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="#">Action</a> <a
											class="dropdown-item" href="#">Another action</a> <a
											class="dropdown-item" href="#">Something else here</a>
									</div>
								</div>
								<div class="text-value">9.823</div>
								<div>User</div>
							</div>
							<div class="chart-wrapper mt-3 mx-3" style="height: 70px;">
								<canvas class="chart" id="card-chart4" height="70"></canvas>
							</div>
						</div>
					</div>
					<!-- /.col-->
				</div>
				<!-- /.row-->
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-5">
								<h4 class="card-title mb-0">Statistics</h4>
								<div class="small text-muted">November 2019</div>
							</div>
							<!-- /.col-->
							<div class="col-sm-7 d-none d-md-block">
								<button class="btn btn-primary float-right" type="button">
									<i class="icon-cloud-download"></i>
								</button>
								<div class="btn-group btn-group-toggle float-right mr-3"
									data-toggle="buttons">
									<label class="btn btn-outline-secondary"> <input
										id="option1" type="radio" name="options" autocomplete="off">
										Day
									</label> <label class="btn btn-outline-secondary active"> <input
										id="option2" type="radio" name="options" autocomplete="off"
										checked=""> Month
									</label> <label class="btn btn-outline-secondary"> <input
										id="option3" type="radio" name="options" autocomplete="off">
										Year
									</label>
								</div>
							</div>
							<!-- /.col-->
						</div>
						<!-- /.row-->
						<div class="chart-wrapper"
							style="height: 300px; margin-top: 40px;">
							<canvas class="chart" id="main-chart" height="300"></canvas>
						</div>
					</div>
					<div class="card-footer">
						<div class="row text-center">
							<div class="col-sm-12 col-md mb-sm-2 mb-0">
								<div class="text-muted">Visits</div>
								<strong>29.703 Users (40%)</strong>
								<div class="progress progress-xs mt-2">
									<div class="progress-bar bg-success" role="progressbar"
										style="width: 40%" aria-valuenow="40" aria-valuemin="0"
										aria-valuemax="100"></div>
								</div>
							</div>
							<div class="col-sm-12 col-md mb-sm-2 mb-0">
								<div class="text-muted">Unique</div>
								<strong>24.093 Users (20%)</strong>
								<div class="progress progress-xs mt-2">
									<div class="progress-bar bg-info" role="progressbar"
										style="width: 20%" aria-valuenow="20" aria-valuemin="0"
										aria-valuemax="100"></div>
								</div>
							</div>
							<div class="col-sm-12 col-md mb-sm-2 mb-0">
								<div class="text-muted">Pageviews</div>
								<strong>78.706 Views (60%)</strong>
								<div class="progress progress-xs mt-2">
									<div class="progress-bar bg-warning" role="progressbar"
										style="width: 60%" aria-valuenow="60" aria-valuemin="0"
										aria-valuemax="100"></div>
								</div>
							</div>
							<div class="col-sm-12 col-md mb-sm-2 mb-0">
								<div class="text-muted">New Users</div>
								<strong>22.123 Users (80%)</strong>
								<div class="progress progress-xs mt-2">
									<div class="progress-bar bg-danger" role="progressbar"
										style="width: 80%" aria-valuenow="80" aria-valuemin="0"
										aria-valuemax="100"></div>
								</div>
							</div>
							<div class="col-sm-12 col-md mb-sm-2 mb-0">
								<div class="text-muted">Bounce Rate</div>
								<strong>40.15%</strong>
								<div class="progress progress-xs mt-2">
									<div class="progress-bar" role="progressbar" style="width: 40%"
										aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.card-->
				<div class="row">
					<div class="col-sm-6 col-lg-3">
						<div class="brand-card">
							<div class="brand-card-header bg-facebook">
								<i class="fa fa-facebook"></i>
								<div class="chart-wrapper">
									<canvas id="social-box-chart-1" height="90"></canvas>
								</div>
							</div>
							<div class="brand-card-body">
								<div>
									<div class="text-value">89k</div>
									<div class="text-uppercase text-muted small">friends</div>
								</div>
								<div>
									<div class="text-value">459</div>
									<div class="text-uppercase text-muted small">feeds</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.col-->
					<div class="col-sm-6 col-lg-3">
						<div class="brand-card">
							<div class="brand-card-header bg-twitter">
								<i class="fa fa-twitter"></i>
								<div class="chart-wrapper">
									<canvas id="social-box-chart-2" height="90"></canvas>
								</div>
							</div>
							<div class="brand-card-body">
								<div>
									<div class="text-value">973k</div>
									<div class="text-uppercase text-muted small">followers</div>
								</div>
								<div>
									<div class="text-value">1.792</div>
									<div class="text-uppercase text-muted small">tweets</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.col-->
					<div class="col-sm-6 col-lg-3">
						<div class="brand-card">
							<div class="brand-card-header bg-linkedin">
								<i class="fa fa-linkedin"></i>
								<div class="chart-wrapper">
									<canvas id="social-box-chart-3" height="90"></canvas>
								</div>
							</div>
							<div class="brand-card-body">
								<div>
									<div class="text-value">500+</div>
									<div class="text-uppercase text-muted small">contacts</div>
								</div>
								<div>
									<div class="text-value">292</div>
									<div class="text-uppercase text-muted small">feeds</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.col-->
					<div class="col-sm-6 col-lg-3">
						<div class="brand-card">
							<div class="brand-card-header bg-google-plus">
								<i class="fa fa-google-plus"></i>
								<div class="chart-wrapper">
									<canvas id="social-box-chart-4" height="90"></canvas>
								</div>
							</div>
							<div class="brand-card-body">
								<div>
									<div class="text-value">894</div>
									<div class="text-uppercase text-muted small">followers</div>
								</div>
								<div>
									<div class="text-value">92</div>
									<div class="text-uppercase text-muted small">circles</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.col-->
				</div>
			</div>
		</div>
		</main>
		<aside class="aside-menu">
			<ul class="nav nav-tabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#timeline" role="tab"> <i
						class="icon-list"></i>
				</a></li>
				<li class="nav-item"><a class="nav-link" data-toggle="tab"
					href="#messages" role="tab"> <i class="icon-speech"></i>
				</a></li>
				<li class="nav-item"><a class="nav-link" data-toggle="tab"
					href="#settings" role="tab"> <i class="icon-settings"></i>
				</a></li>
			</ul>
			<!-- Tab panes-->
			<div class="tab-content">
				<div class="tab-pane active" id="timeline" role="tabpanel">
					<div class="list-group list-group-accent">
						<div
							class="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">Today</div>
						<div
							class="list-group-item list-group-item-accent-warning list-group-item-divider">
							<div class="avatar float-right">
								<img class="img-avatar" src="img/avatars/7.jpg"
									alt="admin@bootstrapmaster.com">
							</div>
							<div>
								Meeting with <strong>Lucas</strong>
							</div>
							<small class="text-muted mr-3"> <i class="icon-calendar"></i> 
								1 - 3pm
							</small> <small class="text-muted"> <i class="icon-location-pin"></i> 
								Palo Alto, CA
							</small>
						</div>
						<div class="list-group-item list-group-item-accent-info">
							<div class="avatar float-right">
								<img class="img-avatar" src="img/avatars/4.jpg"
									alt="admin@bootstrapmaster.com">
							</div>
							<div>
								Skype with <strong>Megan</strong>
							</div>
							<small class="text-muted mr-3"> <i class="icon-calendar"></i> 
								4 - 5pm
							</small> <small class="text-muted"> <i class="icon-social-skype"></i> 
								On-line
							</small>
						</div>
						<div
							class="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">Tomorrow</div>
						<div
							class="list-group-item list-group-item-accent-danger list-group-item-divider">
							<div>
								New UI Project - <strong>deadline</strong>
							</div>
							<small class="text-muted mr-3"> <i class="icon-calendar"></i> 
								10 - 11pm
							</small> <small class="text-muted"> <i class="icon-home"></i> 
								creativeLabs HQ
							</small>
							<div class="avatars-stack mt-2">
								<div class="avatar avatar-xs">
									<img class="img-avatar" src="img/avatars/2.jpg"
										alt="admin@bootstrapmaster.com">
								</div>
								<div class="avatar avatar-xs">
									<img class="img-avatar" src="img/avatars/3.jpg"
										alt="admin@bootstrapmaster.com">
								</div>
								<div class="avatar avatar-xs">
									<img class="img-avatar" src="img/avatars/4.jpg"
										alt="admin@bootstrapmaster.com">
								</div>
								<div class="avatar avatar-xs">
									<img class="img-avatar" src="img/avatars/5.jpg"
										alt="admin@bootstrapmaster.com">
								</div>
								<div class="avatar avatar-xs">
									<img class="img-avatar" src="img/avatars/6.jpg"
										alt="admin@bootstrapmaster.com">
								</div>
							</div>
						</div>
						<div
							class="list-group-item list-group-item-accent-success list-group-item-divider">
							<div>
								<strong>#10 Startups.Garden</strong> Meetup
							</div>
							<small class="text-muted mr-3"> <i class="icon-calendar"></i> 
								1 - 3pm
							</small> <small class="text-muted"> <i class="icon-location-pin"></i> 
								Palo Alto, CA
							</small>
						</div>
						<div
							class="list-group-item list-group-item-accent-primary list-group-item-divider">
							<div>
								<strong>Team meeting</strong>
							</div>
							<small class="text-muted mr-3"> <i class="icon-calendar"></i> 
								4 - 6pm
							</small> <small class="text-muted"> <i class="icon-home"></i> 
								creativeLabs HQ
							</small>
							<div class="avatars-stack mt-2">
								<div class="avatar avatar-xs">
									<img class="img-avatar" src="img/avatars/2.jpg"
										alt="admin@bootstrapmaster.com">
								</div>
								<div class="avatar avatar-xs">
									<img class="img-avatar" src="img/avatars/3.jpg"
										alt="admin@bootstrapmaster.com">
								</div>
								<div class="avatar avatar-xs">
									<img class="img-avatar" src="img/avatars/4.jpg"
										alt="admin@bootstrapmaster.com">
								</div>
								<div class="avatar avatar-xs">
									<img class="img-avatar" src="img/avatars/5.jpg"
										alt="admin@bootstrapmaster.com">
								</div>
								<div class="avatar avatar-xs">
									<img class="img-avatar" src="img/avatars/6.jpg"
										alt="admin@bootstrapmaster.com">
								</div>
								<div class="avatar avatar-xs">
									<img class="img-avatar" src="img/avatars/7.jpg"
										alt="admin@bootstrapmaster.com">
								</div>
								<div class="avatar avatar-xs">
									<img class="img-avatar" src="img/avatars/8.jpg"
										alt="admin@bootstrapmaster.com">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane p-3" id="messages" role="tabpanel">
					<div class="message">
						<div class="py-3 pb-5 mr-3 float-left">
							<div class="avatar">
								<img class="img-avatar" src="img/avatars/7.jpg"
									alt="admin@bootstrapmaster.com"> <span
									class="avatar-status badge-success"></span>
							</div>
						</div>
						<div>
							<small class="text-muted">Lukasz Holeczek</small> <small
								class="text-muted float-right mt-1">1:52 PM</small>
						</div>
						<div class="text-truncate font-weight-bold">Lorem ipsum
							dolor sit amet</div>
						<small class="text-muted">Lorem ipsum dolor sit amet,
							consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
					</div>
					<hr>
					<div class="message">
						<div class="py-3 pb-5 mr-3 float-left">
							<div class="avatar">
								<img class="img-avatar" src="img/avatars/7.jpg"
									alt="admin@bootstrapmaster.com"> <span
									class="avatar-status badge-success"></span>
							</div>
						</div>
						<div>
							<small class="text-muted">Lukasz Holeczek</small> <small
								class="text-muted float-right mt-1">1:52 PM</small>
						</div>
						<div class="text-truncate font-weight-bold">Lorem ipsum
							dolor sit amet</div>
						<small class="text-muted">Lorem ipsum dolor sit amet,
							consectetur adipisicing elit, sed do e	iusmod tempor incididunt...</small>
					</div>
					<hr>
					<div class="message">
						<div class="py-3 pb-5 mr-3 float-left">
							<div class="avatar">
								<img class="img-avatar" src="img/avatars/7.jpg"
									alt="admin@bootstrapmaster.com"> <span
									class="avatar-status badge-success"></span>
							</div>
						</div>
						<div>
							<small class="text-muted">Lukasz Holeczek</small> <small
								class="text-muted float-right mt-1">1:52 PM</small>
						</div>
						<div class="text-truncate font-weight-bold">Lorem ipsum
							dolor sit amet</div>
						<small class="text-muted">Lorem ipsum dolor sit amet,
							consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
					</div>
					<hr>
					<div class="message">
						<div class="py-3 pb-5 mr-3 float-left">
							<div class="avatar">
								<img class="img-avatar" src="img/avatars/7.jpg"
									alt="admin@bootstrapmaster.com"> <span
									class="avatar-status badge-success"></span>
							</div>
						</div>
						<div>
							<small class="text-muted">Lukasz Holeczek</small> <small
								class="text-muted float-right mt-1">1:52 PM</small>
						</div>
						<div class="text-truncate font-weight-bold">Lorem ipsum
							dolor sit amet</div>
						<small class="text-muted">Lorem ipsum dolor sit amet,
							consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
					</div>
					<hr>
					<div class="message">
						<div class="py-3 pb-5 mr-3 float-left">
							<div class="avatar">
								<img class="img-avatar" src="img/avatars/7.jpg"
									alt="admin@bootstrapmaster.com"> <span
									class="avatar-status badge-success"></span>
							</div>
						</div>
						<div>
							<small class="text-muted">Lukasz Holeczek</small> <small
								class="text-muted float-right mt-1">1:52 PM</small>
						</div>
						<div class="text-truncate font-weight-bold">Lorem ipsum
							dolor sit amet</div>
						<small class="text-muted">Lorem ipsum dolor sit amet,
							consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
					</div>
				</div>			
			</div>
		</aside>
	</div>
	<footer class="app-footer">
		<div>
			<a href="https://coreui.io">CoreUI</a> <span>&copy; 2018
				creativeLabs.</span>
		</div>
		<div class="ml-auto">
			<span>Powered by</span> <a href="https://coreui.io">CoreUI</a>
		</div>
	</footer>
</body>
</html>