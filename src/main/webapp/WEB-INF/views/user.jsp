<html lang="en">
 <head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<title>User</title>
<% long timestamp = new java.util.Date().getTime(); %>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/all.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" />

<!-- Main styles for this application-->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/jquery-ui/jquery-ui-1.8.20.custom.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/style.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/header-card.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/vendors/pace-progress/css/pace.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/bootstrap.min.css">
<script type="text/javascript" src="${controllerUtils.dynamicConfigAction}"></script>
<script type="text/javascript" src="${controllerUtils.generatorUrlAjaxAction}"></script>

<!-- CoreUI and necessary plugins-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/jquery.ui/jquery-ui-1.8.20.custom.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/pace.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/header-card.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/momentjs/moment.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/jquery.json-2.3.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/header-card.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.2/underscore-min.js"></script>
<!-- Plugins and scripts required by this view-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/custom-tooltips.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/sweetalert/sweetalert.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/script/common/sweetalert/sweetalert.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/app.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style/common/datatables/dataTables.bootstrap4.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/ui-bootstrap-tpls-2.5.0.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/user/user.service.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/user/user.controller.js"></script>


<script>
	window.dataLayer = window.dataLayer || [];
	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());
	// Shared ID
	gtag('config', 'UA-118965717-3');
	// Bootstrap ID
	gtag('config', 'UA-118965717-5');
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/common/baseui.min.js"></script>
</head>
<body>
<div id="page-content" class="container-fluid" ng-app="myApp" ng-controller="UserController" style="padding-left: 0px; padding-right: 0px;">
	<div id="ui-view">
		<div class="animated fadeIn">
			<div class="card">
				<div class="card-header">
					<div class="dataTables_wrapper dt-bootstrap4 no-footer">
						<div class="row align-items-center">
							<div class="col-sm-12 col-md-3">
								<form ng-submit="search()" class="form-inline">
									<div class="input-group">
										<div class="input-group-prepend">
											<select id="selectSearchType" class="form-control">
												<option value='username'><spring:message code="frontend.view.user.search.username" /></option>
												<option value='name'><spring:message code="frontend.view.user.form.name" /></option>
											</select>
										</div>
										<input type="hidden" name="search_param" value="all" id="search_param"> <input type="text" class="form-control" id="searchKey" placeholder="Search by...">
										<div class="input-group-append">
											<span class="input-group-btn">
												<button class="btn btn-block btn-outline-success" type="submit">
													<i class="fa fa-search" aria-hidden="true"></i>
												</button>
											</span>
										</div>
									</div>
								</form>
							</div>
							<div class="col-sm-12 col-md-5">
								<div class="row align-items-center">
									<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
									</div>
									<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
										<button 
											class="btn btn-success btn-sm" type="button" data-toggle="modal"
											data-target="#addUserModal">
											<i class="fas fa-plus"></i>
											<spring:message code="frontend.view.button.add" />
										</button>
									</div>
									<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
										<button 
											class="btn btn-danger btn-sm" type="button" 
											ng-click="deleteAllUser()">
											<i class="fas fa-times"></i>
											<spring:message code="frontend.view.button.delete" />
										</button>
									</div>
									<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
										<a href="/final-excercise/admin"><button 
											class="btn btn-info btn-sm" type="button">
											<i class="fa fa-home"></i>
											<spring:message code="frontend.view.button.home" />
										</button> </a>
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-md-4">
								<div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
									<ul class="pagination" uib-pagination total-items="total_items" ng-model="current_page" max-size="max_size" boundary-links="true" force-ellipses="true" ng-change="pageChanged()" items-per-page="page_size" first-text="{{paginationFirstText}}" previous-text="{{paginationPreviousText}}" next-text="{{paginationNextText}}" last-text="{{paginationLastText}}">
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-body" style = "max-height: 84vh; overflow: auto;">
					<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
						<div class="row">
							<div id="UserList" class="col-sm-12">
								<table class="table table-striped  table-responsive-sm table-bordered datatable dataTable no-footer table-hover" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
									<thead>
										<tr role="row">
											<th style="width: 7%;">
												<div class="pretty p-image p-plain">
													<input type="checkbox" ng-model="selectedAll" ng-change="selectAll()" />
													<div class="state">
														<img class="image" src="${pageContext.request.contextPath}/resources/imgs/004.png"><label></label>
													</div>
												</div>
											</th>
											<th aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 15%;"><spring:message code="frontend.view.user.table.username" /></th>
											<th aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 13%;"><spring:message code="frontend.view.user.table.password" /></th>
											<th aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 15%;"><spring:message code="frontend.view.user.table.name" /></th>
											<th aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 13%;"><spring:message code="frontend.view.user.table.phone" /></th>
											<th aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 15%;"><spring:message code="frontend.view.user.table.email" /></th>
											<th aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 13%;"><spring:message code="frontend.view.user.table.address" /></th>
											<th aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 20%;"><spring:message code="frontend.view.table.action" /></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="(key,value) in data">
											<td ng-cloak>
												<div class="pretty p-image p-plain">
													<input type="checkbox" name="checkedUser" value="{{value.id}}" ng-model='toDelete[value.id]' ng-click="checkIfSelectAll()" />
													<div class="state">
														<img class="image" src="${pageContext.request.contextPath}/resources/imgs/004.png"><label></label>
													</div>
												</div>
											</td>
											<td ng-cloak>{{value.username}}</td>
											<td ng-cloak>{{value.password}}</td>
											<td ng-cloak>{{value.name}}</td>
											<td ng-cloak>{{value.phone}}</td>
											<td ng-cloak>{{value.email}}</td>
											<td ng-cloak>{{value.address}}</td>											
											<td ng-cloak><a class="btn btn-warning updateLink" data-toggle="modal" data-target="#modal-edit-user" ng-click="loadEditUser(value.id)"><span class="fa fa-edit"></span></a> <a ng-click="deleteOneUser(value.id)" <c:if test="${!isAuthorized}">disabled</c:if> class="btn btn-danger deleteLink" data-value="{{ value.id }}" href="javascript:void(0);"><span class="fas fa-trash-alt" style="font-size:128.7%"></span></a></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="modal-edit-user">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-body">
						<form id="userEditForm" ng-submit="updateUser()"
							class="form-horizontal">
							<input type="hidden" id="txtId" value="{{userData.userID}}">
							<div class="form-group">
								<div class="form-group row">
									<label class="col-md-2 col-form-label"><spring:message
											code="frontend.view.user.form.username" /></label>
									<div class="col-md-4">
										<input class="form-control" type="text" id="txtusername"
											value="{{userData.username}} " />
									</div>

									<label class="col-md-2 col-form-label"><spring:message
											code="frontend.view.user.form.password" /></label>
									<div class="col-md-4">
										<input class="form-control" type="text" id="txtpassword"
											value="{{userData.password}}" maxlength="64"
											title="<spring:message code="frontend.validate.message.content.wrong.input.type.html" />" />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-md-2 col-form-label"><spring:message
											code="frontend.view.user.form.name" /></label>
									<div class="col-md-4">
										<input class="form-control" type="text" id="txtName"
											value="{{userData.name}}" />
									</div>

									<label class="col-md-2 col-form-label"><spring:message
											code="frontend.view.user.form.email" /></label>
									<div class="col-md-4">
										<input class="form-control" type="text" id="txtemail"
											value="{{userData.email}}" maxlength="64"
											title="<spring:message code="frontend.validate.message.content.wrong.input.type.html" />" />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-md-2 col-form-label"><spring:message
											code="frontend.view.user.form.address" /></label>
									<div class="col-md-4">
										<input class="form-control" type="text" step="any" min="-180"
											max="180" id="txtaddress" value="{{userData.address}}"
											required
											oninvalid="this.setCustomValidity('<spring:message code="frontend.validate.message.content.wrong.input.type" />')"
											oninput="setCustomValidity('')" /> <span id="longLatError"
											class="help-block" style="line-height: 20px;"></span>
									</div>

									<label class="col-md-2 col-form-label"><spring:message
											code="frontend.view.user.form.phone" /></label>
									<div class="col-md-4">
										<input class="form-control" type="text" id="txtphone"
											value="{{userData.phone}}" maxlength="64"
											"title="<spring:message code="frontend.validate.message.content.wrong.input.type.html" />" />
									</div>
								</div>
								<div class="card-body row align-items-center">
									<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
										<button id="btnSave" type="submit"
											class="btn btn-block btn-outline-danger">
											<spring:message code="frontend.view.button.save" />
											<i class="fa fa-floppy-o" aria-hidden="true"></i>
										</button>
									</div>
									<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
										<button id="btnCancel"
											class="btn btn-block btn-outline-secondary"
											data-dismiss="modal">
											<spring:message code="frontend.view.button.cancel" />
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="addUserModal">
		<div class="modal-dialog modal-xl" style="max-width: 750px">
			<div class="modal-content">
				<div class="modal-body">
					<form id="districtForm" class="col-md-12 commonForm">
						<div class="card-body">
							<div class="form-group row">
								<label class="col-md-2 col-form-label"><spring:message code="frontend.view.user.form.username" /></label>
								<div class="col-md-4">
									<input class="form-control" id="addUserName" type="text" maxlength="64" pattern="[A-Za-z0-9_ ]+" ng-model="UserName" name="UserName" placeholder="<spring:message code="frontend.view.user.form.username" />" autocomplete="name">
								</div>
								
								<label class="col-md-2 col-form-label"><spring:message code="frontend.view.user.form.password" /></label>
								<div class="col-md-4">
									<input class="form-control required" id="addPassword" type="text" maxlength="64" pattern="[A-Za-z0-9_ ]+" ng-model="Password" name="Password" placeholder="<spring:message code="frontend.view.user.form.password" />" autocomplete="name">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-md-2 col-form-label"><spring:message code="frontend.view.user.form.email" /></label>
								<div class="col-md-4">
									<input class="form-control required" id="addEmail" type="text" maxlength="64" ng-model="Email" name="Email" placeholder="<spring:message code="frontend.view.user.form.email" />" autocomplete="name">
								</div>
							</div>
						</div>
						<div class="card-body row align-items-center">
							<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
								<button id="btnSave" ng-click="userAddForm()" class="btn btn-block btn-outline-danger">
									<spring:message code="frontend.view.button.save" />
									<i class="fa fa-floppy-o" aria-hidden="true"></i>
								</button>
							</div>
							<div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
								<button id="btnCancel" class="btn btn-block btn-outline-secondary" data-dismiss="modal">
									<spring:message code="frontend.view.button.cancel" />
								</button>
							</div>
						</div>
						<div class="commonFormControl">
							<div id="resultMsg" class="alert col-md-3"></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</body>