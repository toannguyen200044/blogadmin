var dayWating = 0;
var hourWating = 0;
var minuteWating = 0;
var secondWating = 0;
var refreshTimeWatingIntervalId;
var deleteIds = '';
var curentDpsId;
var refreshTimeWaiting20s = null;
var refreshTimeWaiting10s = null;
var refreshTimeWaiting1s = null;
var isAuto;
var dpsUpdateSecondsElapsed = 0;
var updateProgressCircleTimer = null;
var currentSelectedDpsId = -1;
var currentSelectedDpsDimmingLevel = -1;
var currentSelectedDpsStatus = -1;
var dialogShownType = 1;

function isDottedDecimalId(data) {
    var rule = /[0-9]+.[0-9]+.[0-9]+.[0-9]+/;
    if (!rule.test(data)) {
        return false;
    }
    var parts = data.split('\.');
    if (parts.length != 4) {
        return false;
    }
    var haveNoneDigitalRule = /\D/;
    for (var i = 0; i < parts.length; i++) {
        if (haveNoneDigitalRule.test(parts[i])) {
            return false;
        }
        if (parseInt(parts[i]) < 0 || parseInt(parts[i]) > 255) {
            return false;
        }
    }
    return true;
}

function displayDpsControl() {
    $('.lightSlider').slider({disabled: false});
    $('.sliderButton').removeAttr('disabled');
    $('#refresh').removeAttr('disabled');
    $('.closeButton').removeAttr('disabled');
    $('.lightSlider').css('opacity', '1');
    $('.sliderButton').css('opacity', '1');
    $(".closeButton").css('opacity', '1');
    $("#refresh").css('opacity', '1');
}

function isIntegerNumber(data) {
    var integerNumberRule = /^[0-9]+(.0*)?$/;
    var haveNoneDigitalRule = /\D/;
    if (data == '') {
        return false;
    }
    if (data.split('.').length > 2) {
        return false;
    } else if (data.split('.').length == 2) {
        if (haveNoneDigitalRule.test(data.split('.')[0]) || haveNoneDigitalRule.test(data.split('.')[1])) {
            return false;
        } else {
            return integerNumberRule.test(data);
        }
    } else {
        if (haveNoneDigitalRule.test(data)) {
            return false;
        } else {
            return integerNumberRule.test(data);
        }
    }
}

function isDoubleNumber(data) {
    var doubleNumberRule = /^(-)?[0-9]+(.[0-9]*)?$/;
    var haveNoneDigitalRule = /\D/;
    if (data == '') {
        return false;
    }
    if (data.split('.').length > 2) {
        return false;
    } else if (data.split('.').length == 2) {
        var part1 = data.split('.')[0];
        part1 = part1.replace('-', '');
        if (haveNoneDigitalRule.test(part1) || haveNoneDigitalRule.test(data.split('.')[1])) {
            return false;
        } else {
            return doubleNumberRule.test(data);
        }
    } else {
        if (haveNoneDigitalRule.test(data)) {
            return false;
        } else {
            return doubleNumberRule.test(data);
        }
    }
}

function addErrorModel(errorModel, code, errorMessage) {

    errorModel.validData = false;
    errorModel.listErrorCode[errorModel.listErrorCode.length] = code;
    errorModel.listErrorMessage[errorModel.listErrorMessage.length] = errorMessage;
}

function renderError(labelId, model) {

    if (model.validData == false) {

        $("#" + labelId).html('');

        var errDisplay = '<ul>';
        for (var i = 0; i < model.listErrorMessage.length; i++) {
            errDisplay = errDisplay + '<li code=' + model.listErrorCode[i] + '>' + model.listErrorMessage[i] + '</li>';
        }
        errDisplay = errDisplay + '</ul>';
        $("#" + labelId).append(errDisplay);
        $("#" + labelId).show();
    }
}

function emailcheck(str) {

    var at = "@";
    var dot = ".";
    var lat = str.indexOf(at);
    var lstr = str.length;
    var ldot = str.indexOf(dot);
    if (str.indexOf(at) == -1) {
        return false;
    }

    if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
        return false;
    }

    if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
        return false;
    }

    if (str.indexOf(at, (lat + 1)) != -1) {
        return false;
    }

    if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
        return false;
    }

    if (str.indexOf(dot, (lat + 2)) == -1) {
        return false;
    }

    if (str.indexOf(" ") != -1) {
        return false;
    }

    return true;
}

function isEmail(Email) {
    Email = $.trim(Email);
    var filter = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.([a-z]){2,4})$/;
    if (!filter.test(Email)) {
        return false;
    }
    else return true;
}
function isDateValid(dateValue) {

    if (Object.prototype.toString.call(dateValue) === "[object Date]") {
        // it is a date
        if (isNaN(dateValue.getTime())) {  // d.valueOf() could also work
            // date is not valid
            return false;
        }
        else {
            // date is valid
            return true;
        }
    }
    else {
        // not a date
        return false;
    }
}

String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "");
};

// move all common functions for zone, dps here
if (typeof COMMON_CONSTANT === "undefined") {
	var returnRecords = 50;
} else {
	var returnRecords = COMMON_CONSTANT.totalItemsOnPage;
}

// var returnRecords = 18;

var requestObject = {
    indexRequest: 0,
    returnRecords: returnRecords
};

// function getData
function getData(controllerURL, urls, requestObject, tableObj, templateObj, params) {
    var sortKey = null;
    var ascending = null;
    $.blockUI({message: '<div class="fa-5x"><i class="fas fa-spinner fa-pulse"></i></div>'});
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: controllerURL,
        data: $.toJSON(requestObject),
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown) {
        	$.unblockUI();
        },
        success: function (data) {
        	$.unblockUI();
            // get totalRecords
            var totalRecords = data.totalRecords;
            var indexRequest = data.indexRequest;
            var dataReturnRecords = data.returnRecords;
            var page = Math.ceil(totalRecords / dataReturnRecords);
            var currentPage = Math.ceil(indexRequest
                / returnRecords);
            var pageObject = {
                currentPage: currentPage,
                page: page,
                returnRecords: returnRecords
            };

            $(tableObj).html(''); // Clear #wines div
            $(tableObj).html(
                _.template($(templateObj).html(), {
                    items: data.listResult,
                    pageObject: pageObject
                }));
            
// view issue
            $('.viewIssue').click(function (e) {
//        		ui.widget.showDialog($("#dlg-view-isue"), $(this), true, true);
                                         var dpsId = $(this).attr("data-value");
                                         var requestObject = {
                                             dpsId: dpsId
                                         };
                                         
                                           $.ajax({
                                           type: "POST",
                                           contentType: "application/json; charset=utf-8",
                                           url: AJAX_URL.openIssueControllerViewAction,
                                           data: $.toJSON(requestObject),
                                           dataType: "json",
                                           error: function (jqXHR, textStatus, errorThrown) {
                                           },
                                           async: false,
                                           success: function (msg) {
                                               if (msg.validData) {
                                                  $("#txtDPSName").val(msg.dpsDto.dpsName);
                                                  $("#txtDPSID").val(msg.dpsDto.dottedDecimalDeviceId);
                                                  $("#txtType").val(msg.dpsDto.deviceTypeName);
                                                  $("#txtLampType").val(msg.dpsDto.lampTypeName);
                                                  $("#txtModel").val(msg.dpsDto.model);
                                                  $("#txtZone").val(msg.dpsDto.zoneName);
                                                  $("#txtDPSLat").val(msg.dpsDto.latitude);
                                                  $("#txtDPSLong").val(msg.dpsDto.longitude);
                                                  $("#txtDistrict").val(msg.dpsDto.districtName);
                                                  $("#txtPoleId").val(msg.dpsDto.poleId);
                                                  $("#txtDescription").val(msg.dpsDto.description);
                                                  $("#txtlampPower").val(msg.dpsDto.lampPower);
                                                 $("#txtinstallYear").val(msg.dpsDto.installYear);
                                               }
                                           },
                                       });
                                         var defaultLat;
                                     var defaultLong;
                                      var markerImage = {
                                                 anchor: new google.maps.Point(14, 46),
                                                 url: STATUS_ICON_URL.statusIconWarning40Url
                                         };
                                     if( $("#txtDPSLong").val() != "")
                                     {
                                         defaultLong = $("#txtDPSLong").val();
                                     }
                                     else
                                     {
                                         defaultLong = 103.78657579421997;
                                     }
                                     if( $("#txtDPSLat").val() != "")
                                     {
                                         defaultLat = $("#txtDPSLat").val();
                                     }
                                     else
                                     {
                                         defaultLat = 1.2958470759913252;
                                     }
                     
                                     // Create an array of styles.
                                     var styles = [
                                       {
                                         stylers: [
                                           { lightness: -25 },
                                           { saturation: -50 },
                                           { gamma: 0.75 }
                                         ]
                                       },{
                                         featureType: "road",
                                         elementType: "geometry",
                                         stylers: [
                                           { lightness: 10 },
                                           { visibility: "simplified" }
                                         ]
                                       },{
                                         featureType: "road",
                                         elementType: "labels",
                                         stylers: [
                                           { visibility: "simplified" }
                                         ]
                                       },{
                                         featureType: "landscape.man_made",
                                         elementType: "geometry",
                                         stylers: [
                                           { visibility: "simplified" }
                                         ]
                                       },{
                                         featureType: "poi",
                                         elementType: "labels",
                                         stylers: [
                                           { visibility: "off" }
                                         ]
                                       }
                                     ];
                     
                                     // Create a new StyledMapType
										// object, passing it
        								// the array of styles,
                                     // as well as the name to be
										// displayed on the
        								// map type control.
                                     var styledMap = new google.maps.StyledMapType(styles, {name: "Dark Map"});
                                     
                                     var mapProp = {
                                               center:new google.maps.LatLng(defaultLat,defaultLong),
                                               zoom:30,
                                               scaleControl: false,
                                               zoomControl: true,
                                               streetViewControl: false,
                                               rotateControl: false,
                                               panControl: false,
                     
                                               mapTypeControl: true,
                                               mapTypeControlOptions: {
                                                       position: google.maps.ControlPosition.TOP_LEFT,
                                                       style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                                                       mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
                                                   }, 
                     
                                               mapTypeId:google.maps.MapTypeId.ROADMAP
                                     };
                                     var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
                     
                                     // Associate the styled map with the
										// MapTypeId
        								// and set it to display.
                                     map.mapTypes.set('map_style', styledMap);
                                     map.setMapTypeId('map_style');
                     
                                     
                                     if(defaultLat != "" && defaultLong != "")
                                     {
                                         isCreating = true;
                                         
                                         markerMLC = new google.maps.Marker ({
                                             position: new google.maps.LatLng(defaultLat ,defaultLong), 
                                             draggable: true,
                                             map: map,
                                             title: 'Choose Location', 
                                             icon: markerImage
                                         });
                                     }
                                     google.maps.event.addListener(map, "click", function(event) {
                                         if(isCreating) return;
                                         
                                         isCreating = true;
                                         var lat = event.latLng.lat();
                                         var lng = event.latLng.lng();
                                     
                                         
                                       document.getElementById('txtMlcLat').value = lat;
                                       document.getElementById('txtMlcLong').value = lng;
                                         
                                         markerMLC = new google.maps.Marker ({
                                             position: new google.maps.LatLng(lat, lng), 
                                             draggable: true,
                                             map: map,
                                             title: 'Create device', 
                                             icon: markerImage
                                         });
                                         
                                         /*
											 * google.maps.event.addListener(markerMLC,
											 * "drag", function(event){ lat =
											 * event.latLng.lat(); lng =
											 * event.latLng.lng();
											 * 
											 * document.getElementById('txtDPSLat').value =
											 * lat;
											 * document.getElementById('txtDPSLong').value =
											 * lng; });
											 */
                                     });
                                          $('.buttonCancel').one("click", function (e) {
//                                          ui.widget.closeDialog($("#dlg-view-isue"));
                                      });
                                         
                                     });

            if (controllerURL == AJAX_URL.dpsControllerJsonList || controllerURL == AJAX_URL.dpsControllerDeleteAction
                || controllerURL == AJAX_URL.dpsControllerJsonSearchList) {
                $('.lockModal').modal({backdrop: 'none', keyboard: false, mouse: false})
                $('.lockModal').modal('hide');

            }
            if (data.listResult.length > 0) {
                for (var i = 0, l = data.listResult.length; i < l; i++) {
                    if (data.listResult[i].id == currentSelectedDpsId) {
                        if (data.listResult[i].status != currentSelectedDpsStatus || data.listResult[i].dimmingLvl != currentSelectedDpsDimmingLevel) {
                            if (dialogShownType === 1) {
                                ShowDimmingControl(currentSelectedDpsId);
                            } else {
                                viewDps(currentSelectedDpsId);
                            }
                            resetUpdateProgressCircleTimer();
                        }
                        break;
                    }
                }

                setupPaginations(controllerURL, urls, requestObject, tableObj, templateObj, params);
                setupCheckbox();
                setupDeleteLink(urls.deleteLink, urls, tableObj, templateObj, params);
                setupUpdateLink(urls.updateLink, params.singleValue);
                setupCloneLink(urls.cloneLink, params.singleValue);
                setupCommissionLink(urls.commissionLink, params.singleValue);
               
                if (controllerURL == AJAX_URL.dpsControllerJsonList || controllerURL == AJAX_URL.dpsControllerDeleteAction
                    || controllerURL == AJAX_URL.dpsControllerJsonSearchList) {
                    setupDpsManualLink();
                    var item = $('.commonTable').find($('th[data-sort-key="' + sortKey + '"]'));
                    if (ascending) {
                        item.addClass('headerSortUp');
                    } else {
                        item.addClass('headerSortDown');
                    }
                    setupSortTable(AJAX_URL.dpsControllerJsonSearchList, urls, requestObject, tableObj, templateObj, params);
                }
                if (controllerURL == AJAX_URL.reportControllerJsonFilterList || controllerURL == AJAX_URL.reportControllerJsonSearchList) {
                    var failureReportId = "";
                    $('.addComment').click(function (e) {
//                        ui.widget.showDialog($("#dlg-comment-failure"), $(this), true, true);
                        failureReportId = $(this).attr("data-value");
                        $("#txtComment").val("");
                        $('.buttonCancel').one("click", function (e) {
//                            ui.widget.closeDialog($("#dlg-comment-failure"));
                        });
                        $('.closeButton').one("click", function (e) {
//                            ui.widget.closeDialog($("#dlg-comment-failure"));
                        });
                        $('.buttonSubmit').unbind("click");
                        $('.buttonSubmit').click(function (e) {
                            var comment = $('#txtComment').val();
                            if (comment == "") {
//                                $.gritter.add({
//                                    position: 'bottom-right',
//                                    title: MESSAGE_CONSTANT.dialogTitleFailed,
//                                    text: MESSAGE_CONSTANT.commentNoData,
//                                    class_name: "gritter-light"
//                                });
                                return;
                            }
                            var requestObject = {
                                failureReportId: failureReportId,
                                comment: comment

                            };
                            $.ajax({
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                url: AJAX_URL.failureReportControllerAddCommentAction,
                                data: $.toJSON(requestObject),
                                dataType: "json",
                                error: function (jqXHR, textStatus, errorThrown) {
                                },
                                success: function (msg) {
//                                    ui.widget.closeDialog($("#dlg-comment-failure"));
//                                    $.gritter.add({
//                                        position: 'bottom-right',
//                                        title: MESSAGE_CONSTANT.dialogTitleSuccess,
//                                        text: MESSAGE_CONSTANT.addCommentSuccess,
//                                        class_name: "gritter-light"
//                                    });

                                    $('#latestComment_' + failureReportId).text(comment);
                                }
                            });

                        });
                    });
                    $('.viewComment').click(function (e) {
//                        ui.widget.showDialog($("#dlg-comment-list"), $(this), true, true);
                        $('#dlg-comment-list').children().remove();
                        var failureTypeName = $(this).parent().parent().find('.tdfailuretype').text();
                        var dpsName = $(this).parent().parent().find('.tddpsname').text();
                        var failureReportId = $(this).attr("data-value");
                        var requestObject = {
                            failureReportId: failureReportId
                        };
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: AJAX_URL.failureReportControllerViewListCommentAction,
                            data: $.toJSON(requestObject),
                            dataType: "json",
                            error: function (jqXHR, textStatus, errorThrown) {
                            },
                            success: function (msg) {
                                if (msg.validData) {
                                    $('#dlg-comment-list').append(
                                        '<div id="dialog-title-addTask" class="dialog-title">'
                                        + '<span>' + dpsName + " - " + failureTypeName + '</span>'
                                        + '<div class="dialogbutton closeButton">'
                                        + '<div>&#10006;</div>'
                                        + '</div>'
                                        + '</div>'
                                        + '<div class="comment-list">'
                                        + '</div>'
                                    );
                                    for (var i = 0; i < msg.lstComment.length; i++) {
                                        var createDate = new Date(msg.lstComment[i].createDate);
                                        createDate = (createDate.getMonth() + 1)
                                            + '/' + createDate.getDate()
                                            + '/' + createDate.getFullYear()
                                            + " " + pad(createDate.getHours(), 2)
                                            + ":" + pad(createDate.getMinutes(), 2)
                                            + ":" + pad(createDate.getSeconds(), 2);
                                        $('.comment-list').append(
                                            '<div class="trComment' + i + '">'
                                            + '<div class="divComment">'
                                            + '<span>' + msg.lstComment[i].comment + '</span>'
                                            + '</div>'
                                            + '<div class="userdetail">'
                                            + '<span>' + "Created by " + msg.lstComment[i].userName + ", " + createDate + '</span>'
                                            + '</div>'
                                            + '</div>'
                                        );
                                        if (i % 2 == 0) {
                                            $('.trComment' + i).addClass("divComment1").removeClass("divComment2");
                                            $('.trComment' + i).addClass("divComment1").removeClass("divComment2");
                                        } else {
                                            $('.trComment' + i).addClass("divComment2").removeClass("divComment1");
                                            $('.trComment' + i).addClass("divComment2").removeClass("divComment1");
                                        }
                                    }
                                    $('.closeButton').one("click", function (e) {
//                                        ui.widget.closeDialog($("#dlg-comment-list"));
                                    });

                                }
                            }
                        });
                    });

                }
            } else {
                $('.pagination').hide();
                $(tableObj).append("<label id='notDataFoundLabel' style='padding-left: 30px;'>" + MESSAGE_CONSTANT.noData + "</label>");
            }
        }
    });
};


function setupCheckbox() {
    // setup select all checkbox
    $('.multiSelect').unbind('change');
    $('.multiSelect').change(function (e) {
        e.preventDefault();
        if (this.checked) {
            $('.singleSelect').prop("checked", true);
        } else {
            $('.singleSelect').prop("checked", false);
        }
    });
};

// function setup delete Link
function setupDeleteAllLink(controllerURL, urls, tableObj, templateObj, params) {
    $(".deleteAllLink").click(function (e) {
        e.preventDefault();
        getAllCheckedZoneId();
        if (deleteIds.length != 0) {
            setupModal(deleteIds, controllerURL, urls, tableObj, templateObj, params);
        } else {
            $('#errorModal').modal("show");
        }
    });
};


// setup dialog
function setupModal(ids, controllerURL, urls, tableObj, templateObj, params) {
    $('#confirmModal').modal("show");
    $('#confirmModal').find(".btn-danger").unbind("click");
    $('#confirmModal').find(".btn-danger").click(
        function (e) {
            var isSuccess = true;
            e.preventDefault();
            var extendObj = {};
            while (ids.indexOf('s') > -1) {
                ids = ids.replace('s', '');
            }
            var page = $('.pagination .active').attr('pageValue');
            var index = page * returnRecords;
            requestObject.indexRequest = index;
            extendObj[params.multiValue] = ids;
            if (controllerURL == AJAX_URL.zoneControllerDeleteAction) {
                var zoneCode = $('#zoneCode').val();
                var zoneName = $('#zoneName').val();
                extendObj['zoneCode'] = zoneCode;
                extendObj['zoneName'] = zoneName;
            } else if (controllerURL == AJAX_URL.dpsControllerDeleteAction) {
                var dpsName = $('#txtDPSName').val();
                var dpsId = $('#txtDPSId').val();
                var zoneId;
                if ($('#selectZone option:selected').val() == -1) {
                    zoneId = null;
                } else {
                    zoneId = $('#selectZone option:selected').val();
                }
                var mlcID;
                if ($('#selectMLC option:selected').val() == -1) {
                    mlcID = null;
                } else {
                    mlcID = $('#selectMLC option:selected').val();
                }
                var status;
                if ($('#selectDPSStatus option:selected').val() == -1) {
                    status = null;
                } else {
                    status = $('#selectDPSStatus option:selected').val();
                }
                var year = $('#txtYear').val();
                var lampPower = $('#txtLampPower').val();
                extendObj['dpsName'] = dpsName;
                extendObj['dpsId'] = dpsId;
                extendObj['zoneId'] = zoneId;
                extendObj['mlcID'] = mlcID;
                extendObj['status'] = status;
                extendObj['year'] = year;
            } else if (controllerURL == AJAX_URL.schedulerGridControllerDeleteAction) {
                var zoneId = $('#selectZone option:selected').val();
                extendObj['zoneId'] = zoneId;
            } else if (controllerURL == AJAX_URL.schedulerCalendarControllerDeleteAction) {
                var zoneId = $('#selectZone option:selected').val();
                extendObj['zoneId'] = zoneId;
            } else if (controllerURL == AJAX_URL.dimmingProfileControllerDeleteAction) {
                var name = $('#dimmingProfileName').val();
                extendObj['name'] = name;
            } else if (controllerURL == AJAX_URL.alarmControllerDeleteAction) {
                var alarmName = $('#txtAlarmName').val();
                var zoneId;
                if ($('#selectZone option:selected').val() == -1) {
                    zoneId = null;
                } else {
                    zoneId = $('#selectZone option:selected').val();
                }
                var alarmType;
                if ($('#selectAlarmType option:selected').val() == -1) {
                    alarmType = null;
                } else {
                    alarmType = $('#selectAlarmType option:selected').val();
                }
                extendObj['name'] = alarmName;
                extendObj['zone_id'] = zoneId;
                extendObj['alarm_type_id'] = alarmType;
            } else if (controllerURL == AJAX_URL.addDimmingProfileControllerDeleteDimmingPointAction) {
                var dimmmingPointListIndex = $('#dimmingProfileListIndex').val();
                extendObj['dimmmingPointListIndex'] = dimmmingPointListIndex;
            }
            // for user
            else if (controllerURL == AJAX_URL.userControllerDeleteAction) {
                var UserName = $('#UserName').val();
                var FirstName = $('#FirstName').val();
                var LastName = $('#LastName').val();
                var ProfileID;
                if ($('#ProfileID option:selected').val() == -1) {
                    ProfileID = null;
                } else {
                    ProfileID = $('#ProfileID option:selected').val();
                }
                extendObj['userName'] = UserName;
                extendObj['userFirstName'] = FirstName;
                extendObj['userLastName'] = LastName;
                extendObj['userProfile'] = ProfileID;
            }
            // for userProfile
            else if (controllerURL == AJAX_URL.userProfileControllerDeleteAction) {
                extendObj['userProfileName'] = $('#userProfileNameSearch').val();
            }
            // for Unallocated
            else if (controllerURL == AJAX_URL.unallocatedDpsControllerDeleteAction) {
                extendObj['dottedDecimalDeviceId'] = $('#txtDeviceId').val();
                ;
            }
            // for District
            else if (controllerURL == AJAX_URL.districtControllerDeleteAction) {
                var districtName = $('#districtNameSearch').val();
                extendObj['districtName'] = districtName;
            }
            else if (controllerURL == AJAX_URL.mlcControllerDeleteAction) {
                var mlcId = $('#txtMLCID').val();
                extendObj['mlcId'] = mlcId;
            }
            var deleteRequest = $.extend(requestObject, extendObj);
            if (controllerURL == AJAX_URL.dimmingProfileControllerDeleteAction) {
                getData(controllerURL, urls, deleteRequest, tableObj, templateObj, params);
                $('#confirmModal').modal("hide");
                setTimeout(function () {
                    isSuccess = true;
                    var id;
                    $('.singleSelect').each(function () {
                        id = 's' + $(this).attr("value") + 's';
                        if (ids == $(this).attr("value") + '') {
                            isSuccess = false;
                        } else {
                            if (deleteIds.indexOf(id) > -1) {
                                isSuccess = false;
                            }
                        }
                    });
                    if (isSuccess == false) {
                        isSuccess = false;
//                        $.gritter.add({
//                            position: 'bottom-right',
//                            // (string | mandatory) the heading of the
//							// notification
//                            title: MESSAGE_CONSTANT.dialogTitleError,
//                            // (string | mandatory) the text inside the
//							// notification
//                            text: MESSAGE_CONSTANT.notificationDeleteMappedDimmingProfile,
//                            class_name: "gritter-light"
//                        });
                    }
                }, 500);
            } else if (controllerURL == AJAX_URL.schedulerGridControllerDeleteAction) {
                getData(controllerURL, urls, deleteRequest, tableObj, templateObj, params);
                $('#confirmModal').modal("hide");
                setTimeout(function () {
                    isSuccess = true;
                    var id;
                    $('.singleSelect').each(function () {
                        id = 's' + $(this).attr("value") + 's';
                        if (ids == $(this).attr("value") + '') {
                            isSuccess = false;
                        } else {
                            if (deleteIds.indexOf(id) > -1) {
                                isSuccess = false;
                            }
                        }
                    });
                    if (isSuccess == false) {
//                        $.gritter.add({
//                            position: 'bottom-right',
//                            // (string | mandatory) the heading of the
//							// notification
//                            title: MESSAGE_CONSTANT.dialogTitleError,
//                            // (string | mandatory) the text inside the
//							// notification
//                            text: MESSAGE_CONSTANT.notificationDeleteMappedScheduler,
//                            class_name: "gritter-light"
//                        });
                    }
                }, 500);
            } else if (controllerURL == AJAX_URL.zoneControllerDeleteAction) {
                getData(controllerURL, urls, deleteRequest, tableObj, templateObj, params);
                $('#confirmModal').modal("hide");
                setTimeout(function () {
                    isSuccess = true;
                    var id;
                    $('.singleSelect').each(function () {
                        id = 's' + $(this).attr("value") + 's';
                        if (ids == $(this).attr("value") + '') {
                            isSuccess = false;
                        } else {
                            if (deleteIds.indexOf(id) > -1) {
                                isSuccess = false;
                            }
                        }
                    });
                    if (isSuccess == false) {
//                        $.gritter.add({
//                            position: 'bottom-right',
//                            // (string | mandatory) the heading of the
//							// notification
//                            title: MESSAGE_CONSTANT.dialogTitleError,
//                            // (string | mandatory) the text inside the
//							// notification
//                            text: MESSAGE_CONSTANT.notificationDeleteMappedZone,
//                            class_name: "gritter-light"
//                        });
                    }
                }, 500);

            } else if (controllerURL == AJAX_URL.userProfileControllerDeleteAction) {
                getData(controllerURL, urls, deleteRequest, tableObj, templateObj, params);
                $('#confirmModal').modal("hide");
                setTimeout(function () {
                    isSuccess = true;
                    var id;
                    $('.singleSelect').each(function () {
                        id = 's' + $(this).attr("value") + 's';
                        if (ids == $(this).attr("value") + '') {
                            isSuccess = false;
                        } else {
                            if (deleteIds.indexOf(id) > -1) {
                                isSuccess = false;
                            }
                        }
                    });
                    if (isSuccess == false) {
//                        $.gritter.add({
//                            position: 'bottom-right',
//                            // (string | mandatory) the heading of the
//							// notification
//                            title: MESSAGE_CONSTANT.dialogTitleError,
//                            // (string | mandatory) the text inside the
//							// notification
//                            text: MESSAGE_CONSTANT.notificationDeleteMappedUser,
//                            class_name: "gritter-light"
//                        });
                    }
                }, 500);
            } else if (controllerURL == AJAX_URL.mlcControllerDeleteAction) {
                getData(controllerURL, urls, deleteRequest, tableObj, templateObj, params);
                $('#confirmModal').modal("hide");
                setTimeout(function () {
                    isSuccess = true;
                    var id;
                    $('.singleSelect').each(function () {
                        id = 's' + $(this).attr("value") + 's';
                        if (ids == $(this).attr("value") + '') {
                            isSuccess = false;
                        } else {
                            if (deleteIds.indexOf(id) > -1) {
                                isSuccess = false;
                            }
                        }
                    });
                    if (isSuccess == false) {
//                        $.gritter.add({
//                            position: 'bottom-right',
//                            // (string | mandatory) the heading of the
//							// notification
//                            title: MESSAGE_CONSTANT.dialogTitleError,
//                            // (string | mandatory) the text inside the
//							// notification
//                            text: MESSAGE_CONSTANT.notificationDeleteOnlineMlc,
//                            class_name: "gritter-light"
//                        });
                    }
                }, 500);
            } else if (controllerURL == AJAX_URL.districtControllerDeleteAction) {
                getData(controllerURL, urls, deleteRequest, tableObj, templateObj, params);
                $('#confirmModal').modal("hide");
                setTimeout(function () {
                    isSuccess = true;
                    var id;
                    $('.singleSelect').each(function () {
                        id = 's' + $(this).attr("value") + 's';
                        if (ids == $(this).attr("value") + '') {
                            isSuccess = false;
                        } else {
                            if (deleteIds.indexOf(id) > -1) {
                                isSuccess = false;
                            }
                        }
                    });
                    if (isSuccess == false) {
//                        $.gritter.add({
//                            position: 'bottom-right',
//                            // (string | mandatory) the heading of the
//							// notification
//                            title: MESSAGE_CONSTANT.dialogTitleError,
//                            // (string | mandatory) the text inside the
//							// notification
//                            text: MESSAGE_CONSTANT.notificationDeleteMappedDps,
//                            class_name: "gritter-light"
//                        });
                    }
                }, 500);
            } else {
                getData(controllerURL, urls, deleteRequest, tableObj, templateObj, params);
                $('#confirmModal').modal("hide");
            }
            setTimeout(function () {
                if (isSuccess) {
                    deleteIds = '';
                }
            }, 500);
        });
};

function checkSelectWhenChangePageOfList() {
    $('.singleSelect').each(
        function () {
            var id = $(this).attr("value") + '';
            if (deleteIds.indexOf('s' + id + 's') > -1) {
                $(this).attr("checked", true);
            }
        });
}

function getAllCheckedZoneId() {
    $('.singleSelect').each(
        function () {
            var id = $(this).attr("value") + '';
            if (this.checked) {
                if (deleteIds.indexOf('s' + id + 's') < 0) {
                    if (deleteIds.length > 0) {
                        deleteIds += ",";
                    }
                    deleteIds += 's' + id + 's';
                }
            } else {
                var temp1 = 's' + id + 's' + ',';
                var temp2 = ',' + 's' + id + 's';
                if (deleteIds.indexOf(temp1) > -1) {
                    deleteIds = deleteIds.replace(temp1, '');
                } else if (deleteIds.indexOf(temp2) > -1) {
                    deleteIds = deleteIds.replace(temp2, '');
                } else if (deleteIds.indexOf('s' + id + 's') > -1) {
                    deleteIds = deleteIds.replace('s' + id + 's', '');
                }
            }
        }
    );
}

// setup pagination
function setupPaginations(controllerURL, urls, requestObject, tableObj, templateObj, params) {
    if (controllerURL != AJAX_URL.addDimmingProfileControllerDeleteDimmingPointAction) {
        checkSelectWhenChangePageOfList();
    }
    $('.pagination > a').click(function (e) {
        getAllCheckedZoneId();
        e.preventDefault();
        var page = $(this).attr('pageValue');
        var index = page * returnRecords;
        requestObject.indexRequest = index;
        getData(controllerURL, urls, requestObject, tableObj, templateObj, params);
    });
}

function setupSortTable(controllerURL, urls, requestObject, tableObj, templateObj, params) {
    $('th.header').click(function () {
        requestObject.sortKey = $(this).data('sort-key');
        var asc = Boolean($(this).hasClass('headerSortDown'));
        $('.commonTable th.header').removeClass('headerSortDown').removeClass('headerSortUp');
        if (asc) {
            $(this).addClass('headerSortUp');
        } else {
            $(this).addClass('headerSortDown');
        }
        requestObject.asc = !asc;
        getData(controllerURL, urls, requestObject, tableObj, templateObj, params);
    });
}

// setup updateLink
function setupUpdateLink(url, paramName) {
    $('.updateLink').each(
        function () {
            var id = $(this).attr("data-value");
            $(this).attr(
                "href", url + "?" + paramName + "=" + id);
        });
};

function setupCloneLink(url, paramName) {
    $('.cloneLink').each(
        function () {
            var id = $(this).attr("data-value");
            $(this).attr(
                "href", url + "?" + paramName + "=" + id);
        });
};

function setupCommissionLink(url, paramName) {
    $('.commissionLink').each(
        function () {
            var id = $(this).attr("data-value");
            $(this).attr(
                "href", url + "?" + paramName + "=" + id);
        });
};

function setupDeleteLink(controllerURL, urls, tableObj, templateObj, params) {
    $(".deleteLink").unbind('click');
    $(".deleteLink").click(function (e) {
        e.preventDefault();
        var id = $(this).attr("data-value") + '';
        setupModal(id, controllerURL, urls, tableObj, templateObj, params);
    });
};

function setupDpsManualLink() {

    $('.settoauto').unbind('click');
    $('.settoauto').click(function (e) {
        e.preventDefault();
        isAuto = true;
        console.log(this);
        console.log(e);
        var dpsId = $(this).attr("data-value");
//        ui.widget.showDialog($("#dlg-ajax-loader-set-mode"), $(this), true, true);

        clearInterval(refreshTimeWaiting1s);
        clearInterval(refreshTimeWaiting10s);

        refreshTimeWaiting10s = setTimeout(function () {
//            $.gritter.add({
//                position: 'bottom-right',
//                title: MESSAGE_CONSTANT.dialogTitleFailed,
//                text: MESSAGE_CONSTANT.changeModeDpsFailed,
//                class_name: "gritter-light"
//            });
//            ui.widget.closeDialog($("#dlg-ajax-loader-set-mode"));
            clearInterval(refreshTimeWaiting1s);
            clearInterval(refreshTimeWaiting10s);
            setStateDpsError(dpsId);
        }, COMMON_CONSTANT.changeDimmingLevelRefreshTime * 1000);

        changeModeDPS(dpsId, "true");

        refreshTimeWaiting1s = setInterval(function () {
            checkChangeModeDPS(dpsId, "true");
        }, COMMON_CONSTANT.dpsCheckChangeModeIntervalTime * 1000);
    });


    $('.manualLink').unbind('click');
    $('.manualLink').click(function (e) {
        if ($('.manualLink[data-value=' + $(this).attr("data-value") + ']').text().trim() == MESSAGE_CONSTANT.setToManual) {
            e.preventDefault();
            isAuto = false;
            var dpsId = $(this).attr("data-value");
//            ui.widget.showDialog($("#dlg-ajax-loader-set-mode"), $(this), true, true);

            clearInterval(refreshTimeWaiting1s);
            clearInterval(refreshTimeWaiting10s);

            refreshTimeWaiting10s = setTimeout(function () {
//                $.gritter.add({
//                    position: 'bottom-right',
//                    title: MESSAGE_CONSTANT.dialogTitleFailed,
//                    text: MESSAGE_CONSTANT.changeModeDpsFailed,
//                    class_name: "gritter-light"
//                });
//                ui.widget.closeDialog($("#dlg-ajax-loader-set-mode"));
                clearInterval(refreshTimeWaiting1s);
                clearInterval(refreshTimeWaiting10s);
            }, COMMON_CONSTANT.changeDimmingLevelRefreshTime * 1000);

            changeModeDPS(dpsId, "false");

            refreshTimeWaiting1s = setInterval(function () {
                checkChangeModeDPS(dpsId, "false");
            }, COMMON_CONSTANT.dpsCheckChangeModeIntervalTime * 1000);
        }
        else {
            resetUpdateProgressCircleTimer();
            ShowDimmingControl($(this).attr("data-value"));
        }
    });
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function resetUpdateProgressCircleTimer() {
    dpsUpdateSecondsElapsed = 0;
    updateControlDimmingIcon();

    clearInterval(updateProgressCircleTimer);
    updateProgressCircleTimer = setInterval(function () {
        dpsUpdateSecondsElapsed++;
        updateControlDimmingIcon();
    }, 1000);
}

function ShowInformationDps(msg) {
	console.log(msg);
	if(msg==null){
		return;
	}
	var dpsDto = msg.dpsDto;
    currentSelectedDpsDimmingLevel = dpsDto!=null?dpsDto.dimmingLevel:0;
    currentSelectedDpsStatus = msg.dpsDto.status;
    dpsDto.controlDimming == null ? dpsDto.controlDimming = dpsDto.dimmingLevel : dpsDto.controlDimming;
    $('#dlg-light-info_lightSlider').attr('updating', 'yes');
    $('#dlg-light-info_lightSlider').slider("value", dpsDto.controlDimming);
    $('#dlg-light-info_lightSlider').attr('updating', 'no');

    $('#deviceNameManualSpan').html(dpsDto.dpsName);
    var status = dpsDto.status;
    var dpsStatus;
    if (status == 'UNKNOWN_STATE') {
        dpsStatus = MESSAGE_CONSTANT.dpsStatus0;
    } else if (status == 'OFF') {
        dpsStatus = MESSAGE_CONSTANT.dpsStatus1;
    } else if (status == 'ON') {
        dpsStatus = MESSAGE_CONSTANT.dpsStatus2;
    } else if (status == 'DIM') {
        dpsStatus = MESSAGE_CONSTANT.dpsStatus3;
    } else if (status == 'WARNING') {
        dpsStatus = MESSAGE_CONSTANT.dpsStatus5;
    } else if (status == 'ERROR') {
        dpsStatus = MESSAGE_CONSTANT.dpsStatus7;
    } else if (status == 'OFFLINE') {
        dpsStatus = MESSAGE_CONSTANT.dpsStatus8;
    } else {
        dpsStatus = '';
    }
    if (dpsDto.failures == null) {
        dpsDto.failures = '';
    }
    $('#lampStatusManual').html(dpsStatus + " " + dpsDto.failures);
    var currentTime = new Date();
    var day = currentTime.getDate();
    var month = currentTime.getMonth() + 1;
    var year = currentTime.getFullYear();
    var hour = currentTime.getHours();
    var minute = currentTime.getMinutes();
    var second = currentTime.getSeconds();
    var timeStamp = day + "/" + month + "/" + year + " " + pad(hour, 2) + ":" + pad(minute, 2) + ":" + pad(second, 2);
    $('#timeStampManual').html(timeStamp);

    // information
    $('#infoDeviceName').html(dpsDto.dpsName);
    $('#dpsDeviceIdManual').val(dpsDto.deviceId);
    $('#infoDpsId').html(dpsDto.dottedDecimalDeviceId);
    $('#infoZone').html(dpsDto.zoneName);
    $('#infoLongitude').html(dpsDto.longitude);
    $('#infoLatitude').html(dpsDto.latitude);
    $('#infoDeviceType').html(dpsDto.deviceTypeName);
    $('#infoLampType').html(dpsDto.lampTypeName);
    $('#infoLampPower').html(dpsDto.lampPower);
    $('#infoInstallYear').html(dpsDto.installYear == 0 ? "" :dpsDto.installYear);
    $('#infoStreet').html(dpsDto.street);
    $('#infoModel').html(dpsDto.model);
    $('#infoFirmwareVersion').html(dpsDto.firmwareVersion);
    dialogShownType = 1;
    drawControlDimmingIcon(msg);

    // electric data
    $('#infoDimmingLevel').html(dpsDto.dimmingLevel);
    if (dpsDto.temperature != null) {
        $('#infoTemperature').html(dpsDto.temperature + "C");
    } else {
        $('#infoTemperature').html('');
    }
    if (dpsDto.outputVoltage != null) {
        $('#infoOutputVoltage').html(dpsDto.outputVoltage / 10 + " V");
    } else {
        $('#infoOutputVoltage').html('');
    }
    if (dpsDto.outputCurrent != null) {
        $('#infoOutputCurrent').html(dpsDto.outputCurrent + " mA");
    } else {
        $('#infoOutputCurrent').html('');
    }
    if (dpsDto.inputVoltage != null) {
        $('#infoInputVoltage').html(dpsDto.inputVoltage / 10 + " V");
    } else {
        $('#infoInputVoltage').html('');
    }
    if (dpsDto.inputCurrent != null) {
        $('#infoInputCurrent').html(dpsDto.inputCurrent + " mA");
    } else {
        $('#infoInputCurrent').html('');
    }
    if (dpsDto.activePower != null) {
        $('#infoActionPower').html(dpsDto.activePower / 10 + " W");
    } else {
        $('#infoActionPower').html('');
    }
    if (dpsDto.powerFactor != null) {
        $('#infoPowerFactor').html(dpsDto.powerFactor + "%");
    } else {
        $('#infoPowerFactor').html('');
    }
    $('#infoStatus').html(dpsStatus);
    if (dpsDto.powerConsummed != null) {
        $('#infoPowerConsumed').html(dpsDto.powerConsummed / 10 + " Wh");
    } else {
        $('#infoPowerConsumed').html('');
    }
//    ui.widget.showDialog($("#dlg-light-info"), $(this), true, true);
    var top = parseInt($("#dlg-light-info").css('top'));
    if (top < 0) {
        $("#dlg-light-info").css('top', '0px');
    }
    $("#dlg-light-info").modal('show');
//    ui.widget.hideAjaxLoader();
//    ui.widget.showOverlay();
}

function ShowDimmingControl(dpsId) {
    curentDpsId = dpsId;
    
    // Refresh after open dimming dialog
//    ui.widget.showAjaxLoader();
    var dpsInfoRequestObject = {
        dpsId: dpsId
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: AJAX_URL.homeControllerJsonLightInfoRefresh,
        data: $.toJSON(dpsInfoRequestObject),
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown) {
//            ui.widget.hideAjaxLoader();
//            ui.widget.hideOverlay();
        },
        success: function (msg) {
            ShowInformationDps(msg);
        }
    });

}
var count = 1;
function changeModeDPS(dpsId, isMode) {
	
    count++;
    var requestObject = {
        dpsIds: dpsId,
        mode: isMode
    };
    console.log('changeModeDPS: ',requestObject);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: AJAX_URL.homeChangeModeDpsController,
        data: $.toJSON(requestObject),
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown) {
        	console.log(errorThrown);
//            $.gritter.add({
//                position: 'bottom-right',
//                title: MESSAGE_CONSTANT.dialogTitleError,
//                text: MESSAGE_CONSTANT.changeModeDpsFailed,
//                class_name: "gritter-light"
//            });
        },
        success: function (msg) {
        	console.log(msg);
            if (msg.validData) {
                return true;
            }
//                $.gritter.add({
//                    position: 'bottom-right',
//                    title: MESSAGE_CONSTANT.dialogTitleError,
//                    text: msg.listErrorMessage[0],
//                    class_name: "gritter-light"
//                });
            swal("missing modbusNum", null, "error");
            return false;
        }
    });
    return true;
}

// set state dps success
function setStateDpsError(curentDpsIds) {
    var requestObject = {
        dpsIds: curentDpsIds
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: AJAX_URL.homeControllerSetStateDps,
        data: $.toJSON(requestObject),
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (msg) {
        }
    });
}

function getControlDimmingCanvasContext() {
    var element;
    if (dialogShownType == 1) {
        element = $('#ControlDimmingIcon');
    } else {
        element = $('#ControlDimmingIcon', '#dlg-view-light-info');
    }
    if (!element.length) return null;
    return element[0].getContext("2d");
}

function drawControlDimmingIcon(msg) {
    clearControlDimmingIcon();

    var ctx = getControlDimmingCanvasContext();
    if (!ctx) return;
    var img = new Image();
    var offset = 5;
    img.src = getControlDimmingStatusImgUrl(msg.dpsDto.status, msg.dpsDto.dimmingLevel);
    img.onload = function () {
        ctx.drawImage(img, offset, offset);
    };
}

function clearControlDimmingIcon() {
    var context = getControlDimmingCanvasContext();
    if (!context) return;
    var offset = 5;
    var centerX = 39, centerY = 39, radius = 39;
    context.beginPath();
    context.arc(centerX + offset, centerY + offset, radius, 0, 2 * Math.PI);
    context.fillStyle = 'white';
    context.fill();
}

function updateControlDimmingIcon() {

    clearDpsUpdateIndicator();

    var context = getControlDimmingCanvasContext();
    if (!context) return;
    if (dpsUpdateSecondsElapsed > COMMON_CONSTANT.maxDpsUpdateDisplayTime) {
        dpsUpdateSecondsElapsed = 0;
    }
    context.lineWidth = 4;
    context.strokeStyle = 'lightblue';

    var offset = 5;
    var centerX = 39, centerY = 39, radius = 42;
    var startAngle = 1.5 * Math.PI;
    var endAngle = (1.5 + (2 * (dpsUpdateSecondsElapsed / COMMON_CONSTANT.maxDpsUpdateDisplayTime) * 100) / 100) * Math.PI;
    context.beginPath();
    context.arc(centerX + offset, centerY + offset, radius, startAngle, endAngle);
    context.stroke();
}

function clearDpsUpdateIndicator() {
    var context = getControlDimmingCanvasContext();
    if (!context) return;
    context.lineWidth = 9;
    context.strokeStyle = 'white';
    var offset = 5;
    var centerX = 39, centerY = 39, radius = 44;
    context.beginPath();
    context.arc(centerX + offset, centerY + offset, radius, 0, 2 * Math.PI);
    context.stroke();
}

// check DPS change state

function checkChangeStateDPS(curentDpsId) {
    var requestObject = {
        dpsId: curentDpsId
    };

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: AJAX_URL.homeControllercheckChangeState,
        data: $.toJSON(requestObject),
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (msg) {
            // if refresh dps success
            if (msg.dpsDto.state == 0) {
                clearInterval(refreshTimeWaiting1s);
                clearInterval(refreshTimeWaiting20s);
                $('#resultMessage').html(MESSAGE_CONSTANT.refreshDpsSuccess);
                $('#resultMessage').css('color', 'Black');
                $("#refresh").css('opacity', '1');
                $("#dlg-light-info .closeButton").css('opacity', '1');
                $('#dlg-light-info .lightSlider').css('opacity', '1');
                $('#dlg-light-info .sliderButton').css('opacity', '1');
                $('#dlg-light-info .lightSlider').removeAttr('disabled');
                $('#dlg-light-info .sliderButton').removeAttr('disabled');
                $('#dlg-light-info .closeButton').removeAttr('disabled');
                $('#refresh').removeAttr('disabled');
                ShowDimmingControl(msg.dpsDto.deviceId);
            }
            // if refresh dps failed
            else if (msg.dpsDto.state == 2) {
                clearInterval(refreshTimeWaiting1s);
                clearInterval(refreshTimeWaiting20s);
                $('#resultMessage').html(MESSAGE_CONSTANT.refreshDpsFailed);
                $('#resultMessage').css('color', 'Red');
                $("#refresh").css('opacity', '1');
                $("#dlg-light-info .closeButton").css('opacity', '1');
                $('#dlg-light-info .lightSlider').css('opacity', '1');
                $('#dlg-light-info .sliderButton').css('opacity', '1');
                $('#dlg-light-info .lightSlider').removeAttr('disabled');
                $('#dlg-light-info .sliderButton').removeAttr('disabled');
                $('#dlg-light-info .closeButton').removeAttr('disabled');
                $('#refresh').removeAttr('disabled');
            }
        }
    });
}

// check DPS change database
function checkChangeModeDPS(dpsId, isMode) {
    var requestObject = {
        dpsIds: dpsId,
        mode: isMode
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: AJAX_URL.homeCheckChangeModeDpsController,
        data: $.toJSON(requestObject),
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown) {
        	$.unblockUI();
            clearInterval(refreshTimeWaiting1s);
            clearInterval(refreshTimeWaiting10s);
//            ui.widget.closeDialog($("#dlg-ajax-loader-set-mode"));
        },
        success: function (msg) {
            if (msg.validData) {
                // check change mode success
                if (msg.dpsDto.state == 0) {
                	$.unblockUI();
                    clearInterval(refreshTimeWaiting1s);
                    clearInterval(refreshTimeWaiting10s);
                    var dimming = msg.dpsDto.controlDimming;
                    if (msg.dpsDto.controlMode) {
                        $('.manualLink[data-value=' + dpsId + ']').html(MESSAGE_CONSTANT.setToManual);
                        $('.settoauto[data-value=' + dpsId + ']').attr('disabled', 'disabled');
                    } else {
                        $('.manualLink[data-value=' + dpsId + ']').html(MESSAGE_CONSTANT.controlManual);
                        $('.settoauto[data-value=' + dpsId + ']').removeAttr('disabled');
                        ShowDimmingControl(dpsId);
                    }
                }
                // check change mode Error
                if (msg.dpsDto.state == 2) {
                	$.unblockUI();
                    clearInterval(refreshTimeWaiting1s);
                    clearInterval(refreshTimeWaiting10s);
//                    ui.widget.closeDialog($("#dlg-ajax-loader-set-mode"));
//                    $.gritter.add({
//                        position: 'bottom-right',
//                        title: MESSAGE_CONSTANT.dialogTitleFailed,
//                        text: MESSAGE_CONSTANT.changeModeDpsFailed,
//                        class_name: "gritter-light"
//                    });
                }
//            	var scope = angular.element(document.getElementById("page-content")).scope();
//            	scope.$apply(function () {
//            	    scope.init();
//            	});
            }
        }
    });
}


function setTimeWating() {
    var hour;
    var minute;
    var second;
    if (hourWating < 10) {
        hour = '0' + hourWating;
    } else {
        hour = hourWating;
    }
    if (minuteWating < 10) {
        minute = '0' + minuteWating;
    } else {
        minute = minuteWating;
    }
    if (secondWating < 10) {
        second = '0' + secondWating;
    } else {
        second = secondWating;
    }
    var time = hour + ':' + minute + ':' + second;
    var day = '';
    if (dayWating > 0) {
        if (dayWating == 1) {
            day = dayWating + 'day ';
        } else {
            day = dayWating + 'day(s) ';
        }
    }
    var content = day + time;
    $('#disable-time-counter').html(content);
}

function disableWebPage() {
    $('body').css('overflow', 'hidden');
    $('.dialog').each(function () {
//        ui.widget.closeDialog($(this));
    });
//    ui.widget.showDialog($('#dlg-disable-time-counter'), $(this), true, false);
    refreshTimeWatingIntervalId = window.setInterval(function () {
        secondWating++;
        if (secondWating == 60) {
            secondWating = 0;
            minuteWating++;
            if (minuteWating == 60) {
                minuteWating = 0;
                hourWating++;
                if (hourWating == 24) {
                    hourWating = 0;
                    dayWating++;
                }
            }
        }
        setTimeWating();
    }, 1000);
}

function enableWebPage() {
    $('body').css('overflow', 'auto');
//    ui.widget.closeDialog($('#dlg-disable-time-counter'));
    clearInterval(refreshTimeWatingIntervalId);
    dayWating = 0;
    hourWating = 0;
    minuteWating = 0;
    secondWating = 0;
    setTimeWating();
}

String.format = function () {
    // The string containing the format items (e.g. "{0}")
    // will and always has to be the first argument.
    var theString = arguments[0];

    // start with the second argument (i = 1)
    for (var i = 1; i < arguments.length; i++) {
        // "gm" = RegEx options for Global search (more than one instance)
        // and for Multiline search
        var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
        theString = theString.replace(regEx, arguments[i]);
    }

    return theString;
};

function showResulDimmingDialog(element, text, dimminglvl, kind) {
    var finalText = text;
    if (dimminglvl != -1) {
        finalText = String.format(text, dimminglvl);
    }
    if (kind == CHANGE_DIMMING_FAIL) {
        element.css('color', 'Red');
    }
    else {
        element.css('color', 'Black');
    }
    element.html(finalText);
}

function SetVerErr() {
    $('.version').each(function () {
        var elem = $(this);
        elem.css('color', 'red');
        setInterval(function () {
            if (elem.css('visibility') == 'hidden') {
                elem.css('visibility', 'visible');
            } else {
                elem.css('visibility', 'hidden');
            }
        }, 500);
    });
}

$(document).ready(function () {

    // When user has any action on webpage and user session was expired then
	// return users to login when
    $(document).ajaxError(function (event, jqXHR, ajaxSettings, thrownError) {
        if (jqXHR.status == 403) {
            window.location.href = AJAX_URL.loginControllerMainAction;
        }
    });

    $('input,textarea,select').filter('[required]:visible').each(function () {
        var elem = $(this);

        elem.on('invalid', function () {
            var validationMessage = MESSAGE_CONSTANT.validationRequired;
            if (elem.attr('id').indexOf('confirmpassword') > -1) {
                validationMessage = elem.attr('title');
            } else {
                if (elem.attr('type') === 'password') {
                    validationMessage += ', ' + MESSAGE_CONSTANT.validationPassword;
                } else if (elem.attr('pattern')) {
                    validationMessage += ', ' + MESSAGE_CONSTANT.validationPattern;
                }
                if (elem.attr('min')) {
                    validationMessage += ', ' + MESSAGE_CONSTANT.validationMinValue + ' ' + elem.attr('min');
                }
                if (elem.attr('max')) {
                    validationMessage += ', ' + MESSAGE_CONSTANT.validationMaxValue + ' ' + elem.attr('max');
                }
            }
            this.setCustomValidity(validationMessage);
        });

        elem.bind('propertychange change click keyup input paste', function () {
            this.setCustomValidity('');
        });
    });

    // set up ajax
    $.ajaxSetup({type: 'POST'});

    if (typeof versionStr !== 'undefined') {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: AJAX_URL.homeControllerGetVersion,
            data: "",
            dataType: "json",
            error: function (jqXHR, textStatus, errorThrown) {
                SetVerErr();
            },
            success: function (data) {
                if (data.data != versionStr) {
                    SetVerErr();
                }
            }
        });
    }
});
// move Alarm type change from add alarm and edit alarm
function AlarmType_Change() {
    var alarmType = $("#selectAlarmType option:selected").val();
    // Failure ratio
    if (alarmType == 1) {
        $("#dvRatio,#dvFailureType,#dvDistrict").removeClass("hideControl");
        $("#dvMLC_Duration,#dvDigital,#dvAddMLC,#dvAddDPS,#dvLimitFailureDPSs,#dvAddDPSChonse").addClass("hideControl");
        $('#txtLimitDPS').attr('required', false);
        $('#txtDuration').attr('required', false);
        $('#txtRatio').attr('required', true);
    }
    // Failure from chosen DPS
    else if (alarmType == 2) {
        $("#dvFailureType,#dvAddDPSChonse,#dvDistrict").removeClass("hideControl");
        $("#dvRatio,#dvMLC_Duration,#dvDigital,#dvAddMLC,#dvLimitFailureDPSs,#dvAddDPS").addClass("hideControl");
        $('#txtLimitDPS').attr('required', false);
        $('#txtDuration').attr('required', false);
        $('#txtRatio').attr('required', false);
    }
    // Number failures
    else if (alarmType == 3) {
        $("#dvFailureType,#dvAddDPS,#dvLimitFailureDPSs,#dvDistrict").removeClass("hideControl");
        $('#txtLimitDPS').attr('required', true);
        $("#dvRatio,#dvMLC_Duration,#dvDigital,#dvAddMLC,#dvAddDPSChonse").addClass("hideControl");
        $('#txtDuration').attr('required', false);
        $('#txtRatio').attr('required', false);
    }
    // Digital input
    else if (alarmType == 4) {
        $("#dvMLC_Duration,#dvDigital,#dvAddMLC").removeClass("hideControl");
        $("#dvRatio,#dvFailureType,#dvAddDPS,#dvLimitFailureDPSs,#dvAddDPSChonse,#dvDistrict").addClass("hideControl");
        $('#txtLimitDPS').attr('required', false);
        $("#lblDurationUnit").text(" (Seconds)");
        $('#txtDuration').attr('required', true);
        $('#txtRatio').attr('required', false);
    }
    // MLC offline
    else if (alarmType == 5) {
        $("#dvMLC_Duration,#dvAddMLC").removeClass("hideControl");
        $("#dvRatio,#dvDigital,#dvFailureType,#dvAddDPS,#dvLimitFailureDPSs,#dvAddDPSChonse,#dvDistrict").addClass("hideControl");
        $('#txtLimitDPS').attr('required', false);
        $("#lblDurationUnit").text(" (Minutes)");
        $("#txtDuration").attr('max', 2880);
        $('#txtDuration').attr('required', true);
        $('#txtRatio').attr('required', false);
    }
    else {
        $("#dvRatio,#dvMLC_Duration,#dvDigital,#dvFailureType,#dvAddMLC,#dvAddDPS,#dvLimitFailureDPSs,#dvAddDPSChonse,#dvDistrict").addClass("hideControl");
        $('#txtLimitDPS').attr('required', false);
        $('#txtDuration').attr('required', false);
        $('#txtRatio').attr('required', false);
    }
}
// move District change from add alarm and edit alarm
function districtChange() {
	$('#tblUnAssignedDpsChonse').find('table').find("tr:gt(0)").remove();
	$('#tblAssignedDpsChonse').find('table').find("tr:gt(0)").remove();
	$.blockUI({message: '<div class="fa-5x"><i class="fas fa-spinner fa-pulse"></i></div>'});
    var mlcListRequestObject = {
        districtId: $("#selectDistrict").val()
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: AJAX_URL.alarmControllerJsonMlcListByDistrict,
        data: $.toJSON(mlcListRequestObject),
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            return;
        },
        success: function (data) {
        	$.unblockUI();
            if (data.validData == false) {
                alert(data.listErrorMessage[0]);
                return;
            } else {
                var str = "";
                for (var i = 0; i < data.listResult.length; i++) {
                    str += "<tr><td><label><input type=\"checkbox\" class=\"ckbDps mlcId\" value=\"" + data.listResult[i].id + "\"/><span>" + data.listResult[i].dottedDecimalId + "</span></label></td></tr>";
                }
                if (str == '') {
                    $("#tblUnAssignedMlc").html(MESSAGE_CONSTANT.noData);
                    $("#tblUnAssignedMlcChonse").html(MESSAGE_CONSTANT.noData);
                    $("#tblAssignedMlc").html('');
                } else {
                    var tableHtml = '<table><tr style="border-bottom:1px solid #fff;"><td><label><input type="checkbox" class="ckbDps" id="ckbUnAssignedMlcAllChonse"/>' + MESSAGE_CONSTANT.selectAll + '</label></td></tr>' + str + '</table>';
                    $("#tblUnAssignedMlc").html(str == "" ? MESSAGE_CONSTANT.noData : tableHtml);
                    $("#tblUnAssignedMlcChonse").html(str == "" ? MESSAGE_CONSTANT.noData : tableHtml);
                    $("#tblAssignedMlc").html('<table><tr style="border-bottom:1px solid #fff;"><td><label><input type="checkbox" class="ckbDps" id="ckbAssignedMlcAll"/>' + MESSAGE_CONSTANT.selectAll + '</label></td></tr></table>');
                }
            }
        }
    });
}

function districtChangeForDps() {

	$.blockUI({message: '<div class="fa-5x"><i class="fas fa-spinner fa-pulse"></i></div>'});
    var dpsListRequestObject = {
            districtId: $("#selectDistrict").val(),
        };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: AJAX_URL.dpsMongoControllerGetByDistrict,
            data: $.toJSON(dpsListRequestObject),
            dataType: "json",
            error: function (jqXHR, textStatus, errorThrown) {
            	$.unblockUI();
                return;
            },
            success: function (data) {
            	$.unblockUI();
                if (data.validData == false) {
                    alert(data.listErrorMessage[0]);
                    return;
                } else {
                    var str = "";
                    for (var i = 0; i < data.listResult.length; i++) {
                        if (data.listResult[i].deviceId !== null) {
                            str += "<tr><td><label><input type=\"checkbox\" class=\"ckbDps\" value=\"" + data.listResult[i].deviceId + "\"/><span>" + data.listResult[i].dottedDecimalDeviceId + "</span><label></td></tr>";
                        }
                    }
                    if (str == '') {
                        $("#tblUnAssignedDps").html(MESSAGE_CONSTANT.noData);
                        $("#tblAssignedDps").html('');
                    } else {
                        var tableHtml = '<table><tr style="border-bottom:1px solid #fff;"><td><label><input type="checkbox" class="ckbDps" id="ckbUnAssignedDpsAll"/>' + MESSAGE_CONSTANT.selectAll + '</label></td></tr>' + str + '</table>';
                        $("#tblUnAssignedDps").html(tableHtml);
                        $("#tblAssignedDps").html('<table><tr style="border-bottom:1px solid #fff;"><td><label><input type="checkbox" class="ckbDps" id="ckbAssignedDpsAll"/>' + MESSAGE_CONSTANT.selectAll + '</label></td></tr></table>');
                        $(document).ready(function () {
                            $("#ckbUnAssignedDpsAll").click(function () {
                                if ($(this).is(":checked")) {
                                    $("#tblUnAssignedDps input:checkbox").attr('checked', 'checked');
                                }
                                else {
                                    $("#tblUnAssignedDps input:checkbox").removeAttr('checked');
                                }
                            });
                            $("#ckbAssignedDpsAll").click(function () {
                                if ($(this).is(":checked")) {
                                    $("#tblAssignedDps input:checkbox").attr('checked', 'checked');
                                }
                                else {
                                    $("#tblAssignedDps input:checkbox").removeAttr('checked');
                                }
                            });
                        });
                    }
                }
            }
        });
}

function getAllMLCs(){
	var mlcListRequestObject = {};
	    $.ajax({
	        type: "POST",
	        contentType: "application/json; charset=utf-8",
	        url: AJAX_URL.mlcMongoControllerGetAll,
	        data: $.toJSON(mlcListRequestObject),
	        dataType: "json",
	        error: function (jqXHR, textStatus, errorThrown) {
	            alert("error");
	            return;
	        },
	        success: function (data) {
	            if (data.validData == false) {
	                alert(data.listErrorMessage[0]);
	                return;
	            } else {
	                var str = "";
	                for (var i = 0; i < data.listResult.length; i++) {
	                    // if (data.listResult[i].deviceId !== null) {
	                        str += "<tr><td><label><input type=\"checkbox\" class=\"ckbDps mlcId\" value=\"" + data.listResult[i].id + "\"/><span>" + data.listResult[i].dottedDecimalId + "</span></label></td></tr>";
	                    // }
	                }
	                if (str == '') {
	                    $("#tblUnAssignedMlc").html(MESSAGE_CONSTANT.noData);
	                    $("#tblAssignedMlc").html('');
	                } else {
	                    var tableHtml = '<table><tr style="border-bottom:1px solid #fff;"><td><label><input type="checkbox" class="ckbDps" id="ckbUnAssignedMlcAll"/>' + MESSAGE_CONSTANT.selectAll + '</label></td></tr>' + str + '</table>';
	                    $("#tblUnAssignedMlc").html(tableHtml);
	                    $("#tblAssignedMlc").html('<table><tr style="border-bottom:1px solid #fff;"><td><label><input type="checkbox" class="ckbDps" id="ckbAssignedMlcAll"/>' + MESSAGE_CONSTANT.selectAll + '</label></td></tr></table>');
	                    $(document).ready(function () {
	                        $("#ckbUnAssignedMlcAll").click(function () {
	                            if ($(this).is(":checked")) {
	                                $("#tblUnAssignedMlc input:checkbox").attr('checked', 'checked');
	                            }
	                            else {
	                                $("#tblUnAssignedMlc input:checkbox").removeAttr('checked');
	                            }
	                        });
	                        $("#ckbAssignedMlcAll").click(function () {
	                            if ($(this).is(":checked")) {
	                                $("#tblAssignedMlc input:checkbox").attr('checked', 'checked');
	                            }
	                            else {
	                                $("#tblAssignedMlc input:checkbox").removeAttr('checked');
	                            }
	                        });
	                    });
	                }
	            }
	        }
	    });
}
// function get stastus dps
function getDpsStatus(status) {
    if (status == 'UNKNOWN_STATE') {
        dpsStatus = MESSAGE_CONSTANT.dpsStatus0;
    } else if (status == 'OFF') {
        dpsStatus = MESSAGE_CONSTANT.dpsStatus1;
    } else if (status == 'ON') {
        dpsStatus = MESSAGE_CONSTANT.dpsStatus2;
    } else if (status == 'DIM') {
        dpsStatus = MESSAGE_CONSTANT.dpsStatus3;
    } else if (status == 'WARNING') {
        dpsStatus = MESSAGE_CONSTANT.dpsStatus5;
    } else if (status == 'ERROR') {
        dpsStatus = MESSAGE_CONSTANT.dpsStatus7;
    } else if (status == 'OFFLINE') {
        dpsStatus = MESSAGE_CONSTANT.dpsStatus8;
    }
    else {
        dpsStatus = '';
    }
    return dpsStatus;
}

// function get stastus dps
function getMLCStatus(status) {
    if (status == 'CONNECTED') {
        mlcStatus = MESSAGE_CONSTANT.mlcStatusConnected;
    } else if (status == 'OFFLINE') {
        mlctatus = MESSAGE_CONSTANT.mlcStatusOffline;
    } else if (status == 'INIT') {
        mlcStatus = MESSAGE_CONSTANT.mlcStatusInit;
    } else if (status == 'REGISTRATION') {
        mlcStatus = MESSAGE_CONSTANT.mlcStatusRegistration;
    } else if (status == 'BOOTING') {
        mlcStatus = MESSAGE_CONSTANT.mlcStatusBooting;
    } else if (status == 'READY') {
        mlcStatus = MESSAGE_CONSTANT.mlcStatusReady;
    } else if (status == 'PROCESS_EVENT') {
        mlcStatus = MESSAGE_CONSTANT.mlcStatusProcessEvent;
    }
    else {
        mlcStatus = '';
    }
    return mlcStatus;
} 