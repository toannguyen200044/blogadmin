$(document).ready(function() {
	var url = window.location.href;

	$('#menu a').removeClass();
	if (url.indexOf("home") > -1) {
		$('#homeMenuDiv').addClass("activeMenu");
		$('#zoneMenuDiv').addClass("deactiveMenu");
		$('#mlcMenuDiv').addClass("deactiveMenu");
		$('#dpsMenuDiv').addClass("deactiveMenu");
		$('#schedulerMenuDiv').addClass("deactiveMenu");
		$('#alarmMenuDiv').addClass("deactiveMenu");
		$('#dimmingProfileMenuDiv').addClass("deactiveMenu");
		$('#usersMenuDiv').addClass("deactiveMenu");
		$('#usersProfileMenuDiv').addClass("deactiveMenu");
		$('#reportMenuDiv').addClass("deactiveMenu");
		$('#deviceLifetimeMenuDiv').addClass("deactiveMenu");
		$('#eventLogMenuDiv').addClass("deactiveMenu");
	} else if (url.indexOf("zone") > -1) {
		$('#homeMenuDiv').addClass("deactiveMenu");
		$('#zoneMenuDiv').addClass("activeMenu");
		$('#mlcMenuDiv').addClass("deactiveMenu");
		$('#dpsMenuDiv').addClass("deactiveMenu");
		$('#schedulerMenuDiv').addClass("deactiveMenu");
		$('#alarmMenuDiv').addClass("deactiveMenu");
		$('#dimmingProfileMenuDiv').addClass("deactiveMenu");
		$('#usersMenuDiv').addClass("deactiveMenu");
		$('#usersProfileMenuDiv').addClass("deactiveMenu");
		$('#reportMenuDiv').addClass("deactiveMenu");
		$('#deviceLifetimeMenuDiv').addClass("deactiveMenu");
		$('#eventLogMenuDiv').addClass("deactiveMenu");
	} else if (url.indexOf("mlc") > -1) {
		$('#homeMenuDiv').addClass("deactiveMenu");
		$('#zoneMenuDiv').addClass("deactiveMenu");
		$('#mlcMenuDiv').addClass("activeMenu");
		$('#dpsMenuDiv').addClass("deactiveMenu");
		$('#schedulerMenuDiv').addClass("deactiveMenu");
		$('#alarmMenuDiv').addClass("deactiveMenu");
		$('#dimmingProfileMenuDiv').addClass("deactiveMenu");
		$('#usersMenuDiv').addClass("deactiveMenu");
		$('#usersProfileMenuDiv').addClass("deactiveMenu");
		$('#reportMenuDiv').addClass("deactiveMenu");
		$('#deviceLifetimeMenuDiv').addClass("deactiveMenu");
		$('#eventLogMenuDiv').addClass("deactiveMenu");
	} else if (url.indexOf("dps") > -1) {
		$('#homeMenuDiv').addClass("deactiveMenu");
		$('#zoneMenuDiv').addClass("deactiveMenu");
		$('#mlcMenuDiv').addClass("deactiveMenu");
		$('#dpsMenuDiv').addClass("activeMenu");
		$('#schedulerMenuDiv').addClass("deactiveMenu");
		$('#alarmMenuDiv').addClass("deactiveMenu");
		$('#dimmingProfileMenuDiv').addClass("deactiveMenu");
		$('#usersMenuDiv').addClass("deactiveMenu");
		$('#usersProfileMenuDiv').addClass("deactiveMenu");
		$('#reportMenuDiv').addClass("deactiveMenu");
		$('#deviceLifetimeMenuDiv').addClass("deactiveMenu");
		$('#eventLogMenuDiv').addClass("deactiveMenu");
	} else if (url.indexOf("scheduler") > -1) {
		$('#homeMenuDiv').addClass("deactiveMenu");
		$('#zoneMenuDiv').addClass("deactiveMenu");
		$('#mlcMenuDiv').addClass("deactiveMenu");
		$('#dpsMenuDiv').addClass("deactiveMenu");
		$('#schedulerMenuDiv').addClass("activeMenu");
		$('#alarmMenuDiv').addClass("deactiveMenu");
		$('#dimmingProfileMenuDiv').addClass("deactiveMenu");
		$('#usersMenuDiv').addClass("deactiveMenu");
		$('#usersProfileMenuDiv').addClass("deactiveMenu");
		$('#reportMenuDiv').addClass("deactiveMenu");
		$('#deviceLifetimeMenuDiv').addClass("deactiveMenu");
		$('#eventLogMenuDiv').addClass("deactiveMenu");
	} else if (url.indexOf("alarm") > -1) {
		$('#homeMenuDiv').addClass("deactiveMenu");
		$('#zoneMenuDiv').addClass("deactiveMenu");
		$('#mlcMenuDiv').addClass("deactiveMenu");
		$('#dpsMenuDiv').addClass("deactiveMenu");
		$('#schedulerMenuDiv').addClass("deactiveMenu");
		$('#alarmMenuDiv').addClass("activeMenu");
		$('#dimmingProfileMenuDiv').addClass("deactiveMenu");
		$('#usersMenuDiv').addClass("deactiveMenu");
		$('#usersProfileMenuDiv').addClass("deactiveMenu");
		$('#reportMenuDiv').addClass("deactiveMenu");
		$('#deviceLifetimeMenuDiv').addClass("deactiveMenu");
		$('#eventLogMenuDiv').addClass("deactiveMenu");
	} else if (url.indexOf("dimmingprofile") > -1) {
		$('#homeMenuDiv').addClass("deactiveMenu");
		$('#zoneMenuDiv').addClass("deactiveMenu");
		$('#mlcMenuDiv').addClass("deactiveMenu");
		$('#dpsMenuDiv').addClass("deactiveMenu");
		$('#schedulerMenuDiv').addClass("deactiveMenu");
		$('#alarmMenuDiv').addClass("deactiveMenu");
		$('#dimmingProfileMenuDiv').addClass("activeMenu");
		$('#usersMenuDiv').addClass("deactiveMenu");
		$('#usersProfileMenuDiv').addClass("deactiveMenu");
		$('#reportMenuDiv').addClass("deactiveMenu");
		$('#deviceLifetimeMenuDiv').addClass("deactiveMenu");
		$('#eventLogMenuDiv').addClass("deactiveMenu");
	}  else if (url.indexOf("userprofile") > -1) {
		$('#homeMenuDiv').addClass("deactiveMenu");
		$('#zoneMenuDiv').addClass("deactiveMenu");
		$('#mlcMenuDiv').addClass("deactiveMenu");
		$('#dpsMenuDiv').addClass("deactiveMenu");
		$('#schedulerMenuDiv').addClass("deactiveMenu");
		$('#alarmMenuDiv').addClass("deactiveMenu");
		$('#dimmingProfileMenuDiv').addClass("deactiveMenu");
		$('#usersMenuDiv').addClass("deactiveMenu");
		$('#usersProfileMenuDiv').addClass("activeMenu");
		$('#reportMenuDiv').addClass("deactiveMenu");
		$('#deviceLifetimeMenuDiv').addClass("deactiveMenu");
		$('#eventLogMenuDiv').addClass("deactiveMenu");
	} else if (url.indexOf("user") > -1) {
		$('#homeMenuDiv').addClass("deactiveMenu");
		$('#zoneMenuDiv').addClass("deactiveMenu");
		$('#mlcMenuDiv').addClass("deactiveMenu");
		$('#dpsMenuDiv').addClass("deactiveMenu");
		$('#schedulerMenuDiv').addClass("deactiveMenu");
		$('#alarmMenuDiv').addClass("deactiveMenu");
		$('#dimmingProfileMenuDiv').addClass("deactiveMenu");
		$('#usersMenuDiv').addClass("activeMenu");
		$('#usersProfileMenuDiv').addClass("deactiveMenu");
		$('#reportMenuDiv').addClass("deactiveMenu");
		$('#deviceLifetimeMenuDiv').addClass("deactiveMenu");
		$('#eventLogMenuDiv').addClass("deactiveMenu");
	} else if (url.indexOf("report") > -1) {
		$('#homeMenuDiv').addClass("deactiveMenu");
		$('#zoneMenuDiv').addClass("deactiveMenu");
		$('#mlcMenuDiv').addClass("deactiveMenu");
		$('#dpsMenuDiv').addClass("deactiveMenu");
		$('#schedulerMenuDiv').addClass("deactiveMenu");
		$('#alarmMenuDiv').addClass("deactiveMenu");
		$('#dimmingProfileMenuDiv').addClass("deactiveMenu");
		$('#usersMenuDiv').addClass("deactiveMenu");
		$('#usersProfileMenuDiv').addClass("deactiveMenu");
		$('#reportMenuDiv').addClass("activeMenu");
		$('#deviceLifetimeMenuDiv').addClass("deactiveMenu");
		$('#eventLogMenuDiv').addClass("deactiveMenu");
	} else {
		$('#homeMenuDiv').addClass("deactiveMenu");
		$('#zoneMenuDiv').addClass("deactiveMenu");
		$('#mlcMenuDiv').addClass("deactiveMenu");
		$('#dpsMenuDiv').addClass("deactiveMenu");
		$('#schedulerMenuDiv').addClass("deactiveMenu");
		$('#alarmMenuDiv').addClass("deactiveMenu");
		$('#dimmingProfileMenuDiv').addClass("deactiveMenu");
		$('#usersMenuDiv').addClass("deactiveMenu");
		$('#usersProfileMenuDiv').addClass("deactiveMenu");
		$('#reportMenuDiv').addClass("deactiveMenu");
		$('#deviceLifetimeMenuDiv').addClass("activeMenu");
		$('#eventLogMenuDiv').addClass("deactiveMenu");
	}
});