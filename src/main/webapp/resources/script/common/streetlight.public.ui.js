﻿var ui = {
	clazz: {
		active: "active",
		selected: "selected",
		collapsed: "collapsed",
		stackLvl1: "stack-level1",
		stackLvl2: "stack-level2"
	},
	layout: {
		resizeSideContent: function() {
			//var height = $("#main-content").outerHeight();
			//$("#side-content").css({ "min-height": height + "px" });
		},
		resizeMainContent: function() {
			//var width = $("#page-content").outerWidth() - $("#side-content").outerWidth() - 40;
			//$("#main-content").css({ "width": width + "px" });//.show();
			//$("#main-content").trigger("afterShow");
			//alert('resizeMainContent');
			
			var headerHeight = $('#headerPane').outerHeight();
			var footerHeight = $('#footerPane').outerHeight();
			var mainContentHeight = $(window).outerHeight() - headerHeight - footerHeight;
			
			$("#bodyPane").css({ "height": mainContentHeight + "px" });
			
			//$("#filterPane").css({ "width": $(window).outerWidth() + "px" });
		},
		setContentPadding: function() {
			var windowHeight = $(window).height();
			var contentHeight = $("#page-content").outerHeight();
			var headerHeight = $("#page-header").outerHeight();
			var footerHeight = $("#page-footer").outerHeight();
			var extraMargin = (windowHeight - contentHeight - headerHeight - footerHeight) / 2;
			extraMargin = extraMargin < 0 ? 0 : extraMargin;
			$("#page-content").css({ "margin": extraMargin + "px 0" });
		}, 
		setFilterAnimation: function() {
			$('#btnFilter').click(function() {
								
				$('.filterControl').toggleClass('extend', 500);
				$('.filterPane').toggleClass('extend', 500);
			});
			
			$('.collapseContainer .collapseTitle').click(function() {
				
				if ($(this).hasClass("collapse")){
					$('.collapseContainer .collapseTitle').each(function(){
						if (!$(this).hasClass("collapse")){
							$(this).addClass('collapse', 500);
							var content = $(this).parent().find(".collapseContent");
							if (!$(content).hasClass("collapse")){
								$(content).addClass('collapse', 500);
							}
						}
					});
				}
				
				$(this).toggleClass('collapse', 500);
				
				var collapseContent = $(this).parent().find('.collapseContent');
				if(collapseContent.hasClass('collapse')) {
					collapseContent.show();
				}
				
				collapseContent.toggleClass('collapse', 500).promise().done(function(){
				    
					if($(this).hasClass('collapse')) {
						$(this).hide();
					}
				});
			});
		}
	},
	widget: {
		setupHeaderContextMenus: function() {
			$("#instant-notifs .show-notif-button").click(function() {
				$("#instant-notifs .notif-list:hidden").slideDown(settings.effect.speed);
				return false;
			});
			$("#login-account .show-context-button").click(function() {
				$("#login-account .account-context:hidden").slideDown(settings.effect.speed);
				return false;
			});
			$("*").mouseup(function() {
				$("#instant-notifs .notif-list").slideUp(settings.effect.speed);
				$("#login-account .account-context").slideUp(settings.effect.speed);
			});
		},
		setupSideNavigation: function() {
			$("#side-nav .has-sub").click(function() {
				$(this).next(".side-nav-sub").slideToggle(200, function() {
					ui.layout.resizeMainContent();
				});
				return false;
			});
		},
		setupContentBoxes: function() {
			$("#main-content .content-box").find(".content-box-title:has(.toggler)").click(function() {
				var contentBoxTitle = $(this);
				contentBoxTitle.next(".content-box-main").slideToggle(settings.effect.speed, function() {
					contentBoxTitle.toggleClass(ui.clazz.collapsed);
					ui.layout.resizeSideContent();
				});
			});
			$("#main-content .content-box .toggler").click(function() {
				var contentBoxTitle = $(this).closest(".content-box-title");
				contentBoxTitle.next(".content-box-main").slideToggle(settings.effect.speed, function() {
					contentBoxTitle.toggleClass(ui.clazz.collapsed);
					ui.layout.resizeSideContent();
				});
			});
		},
		setupDatePicker: function() {
			var dateInputs = $(".date-input");
			var dateFormat = CONFIG.DATE_ONLY_FORMAT;
			dateFormat = dateFormat.replace("MM", "mm");
			dateFormat = dateFormat.replace("yyyy", "yy");
			
			if (dateInputs.datepicker) {
				dateInputs.next(".icon-button").remove();
				dateInputs.datepicker({
					dateFormat: dateFormat,
					constrainInput: true,
					showOn: "button",
					showAnim: "slideDown",
					showOtherMonths: true,
					changeMonth: true,
					changeYear: true
				});
			}
		},
		setupDialogs: function() {
			var overlay = $("#dialog-overlay");
			// Do nothing if the current page does not have overlay layer
			if (overlay.size() == 0) {
				return;
			}
			// Allow dialog to be moved around the screen
			// Need jQuery UI to perform
			if ($(".dialog").draggable) {
				$(".dialog").draggable({ handle: ".dialog-title", containment: "document" });
			}
			$(".dialog .dialog-close").click(function() {
				var targetDialog = $(this).closest(".dialog");
				ui.widget.closeDialog(targetDialog);
			});
			
			$(".dialog .dialog-title .dialogbutton.closeButton").click(function() {
				if (!( $(this).attr('disabled') == 'disabled'))
				{
					var targetDialog = $(this).closest(".dialog");
					ui.widget.closeDialog(targetDialog);
				}
			});
			
			$(".dialog-trigger").live("click", function() {
				var dialogId = $(this).attr("data-dialog-id");
				var targetDialog = $("#" + dialogId);
				ui.widget.showDialog(targetDialog, $(this));
			});
			// Auto open a dialog if it is specified
			$(".dialog-trigger").each(function() {
				if ($(this).hasClass("auto-open")) {
					$(this).click();
				}
			});
			// Resize overlay div on window resize
			$(window).resize(ui.widget.resizeDialogOverlay);
		},		
		showDialog: function(targetDialog, trigger) {
			// Do nothing if cannot find the target dialog or the trigger
			if (targetDialog.size() == 0 || trigger.size() == 0) {
				return false;
			}
			var overlay = $("#dialog-overlay");
			if (trigger.hasClass(ui.clazz.stackLvl1)) {
				overlay.addClass(ui.clazz.stackLvl1);
				targetDialog.addClass(ui.clazz.stackLvl1);
			}
			else if (trigger.hasClass(ui.clazz.stackLvl2)) {
				overlay.addClass(ui.clazz.stackLvl2);
				targetDialog.addClass(ui.clazz.stackLvl2);
			}
			ui.widget.resizeDialogOverlay();
			overlay.show();
			ui.widget.adjustDialogPosition(targetDialog);
			/*
			targetDialog.fadeIn(settings.effect.speed, function() {
				$(this).find(".trigger-action").each(function() {
					var funcStr = $(this).val();
					window[funcStr]();
				});
				targetDialog.trigger("afterShow");
			});
			*/
			targetDialog.show();
		},
		showDialog: function(targetDialog, trigger, modal) {
			// Do nothing if cannot find the target dialog or the trigger
			if (targetDialog.size() == 0 || trigger.size() == 0) {
				return false;
			}
			var overlay = $("#dialog-overlay");
			if (trigger.hasClass(ui.clazz.stackLvl1)) {
				overlay.addClass(ui.clazz.stackLvl1);
				targetDialog.addClass(ui.clazz.stackLvl1);
			}
			else if (trigger.hasClass(ui.clazz.stackLvl2)) {
				overlay.addClass(ui.clazz.stackLvl2);
				targetDialog.addClass(ui.clazz.stackLvl2);
			}
			ui.widget.resizeDialogOverlay();
			if(modal == true){
				overlay.show();
			}
			ui.widget.adjustDialogPosition(targetDialog);
			/*
			targetDialog.fadeIn(settings.effect.speed, function() {
				$(this).find(".trigger-action").each(function() {
					var funcStr = $(this).val();
					window[funcStr]();
				});
				targetDialog.trigger("afterShow");
			});
			*/
			targetDialog.show();
		},
		showDialog: function(targetDialog, trigger, modal, isTopRightAdjust) {
			// Do nothing if cannot find the target dialog or the trigger
			if (targetDialog.size() == 0 || trigger.size() == 0) {
				return false;
			}
			var overlay = $("#dialog-overlay");
			if (trigger.hasClass(ui.clazz.stackLvl1)) {
				overlay.addClass(ui.clazz.stackLvl1);
				targetDialog.addClass(ui.clazz.stackLvl1);
			}
			else if (trigger.hasClass(ui.clazz.stackLvl2)) {
				overlay.addClass(ui.clazz.stackLvl2);
				targetDialog.addClass(ui.clazz.stackLvl2);
			}
			ui.widget.resizeDialogOverlay();
			if(modal == true){
				overlay.show();
			}
			if(isTopRightAdjust == true){
				ui.widget.adjustDialogTopRightPosition(targetDialog);
			} else {
				ui.widget.adjustDialogPosition(targetDialog);
			}			
			targetDialog.show();
		},
		closeDialog: function(targetDialog)  {
			// Do nothing if cannot find the target dialog
			if (targetDialog.size() == 0) {
				return false;
			}
			var overlay = $("#dialog-overlay");
			
			targetDialog.hide();
			overlay.hide();
			/*
			targetDialog.fadeOut(settings.effect.speed, function() {
				if ($(this).hasClass(ui.clazz.stackLvl2)) {
					$(this).removeClass(ui.clazz.stackLvl2);
				} else {
					$(this).removeClass(ui.clazz.stackLvl1);
				}					
				if (overlay.hasClass(ui.clazz.stackLvl1) || overlay.hasClass(ui.clazz.stackLvl2)) {
					if (overlay.hasClass(ui.clazz.stackLvl2)) {
						overlay.removeClass(ui.clazz.stackLvl2);
					} else {
						overlay.removeClass(ui.clazz.stackLvl1);
					}
				} else {
					overlay.hide();
				}
				targetDialog.trigger("afterClose");
			});
			*/
		},
		adjustDialogTopRightPosition: function (targetDialog, useAnimation){
			var _useAnimation = useAnimation || false;
			var positionObj = {
				"left": (($(window).width() - targetDialog.outerWidth())) + "px",
				"top": 0 + "px"
			};
			if (_useAnimation) {
				targetDialog.animate(positionObj, 250);
			} else {
				targetDialog.css(positionObj);
			}
		},
		adjustDialogPosition: function(targetDialog, useAnimation) {
			var _useAnimation = useAnimation || false;
			var positionObj = {
				//"left": "50%",
				//"margin-left": "-" + targetDialog.outerWidth() / 2 + "px",
				"left": (($(window).width() - targetDialog.outerWidth()) / 2) + "px",
				"top": (($(window).height() - targetDialog.height()) / 2) + "px"
			};
			if (_useAnimation) {
				targetDialog.animate(positionObj, 250);
			} else {
				targetDialog.css(positionObj);
			}
		},
		resizeDialogOverlay: function(context) {
			var winHeight = $(window).height();
			var docHeight = $(document).height();
			var setHeight = docHeight > winHeight ? docHeight : winHeight;				
			$("#dialog-overlay").css({ "height": setHeight + "px" });
		},
		showAjaxLoader: function() {
			var ajaxLoader = $("#dlg-ajax-loader");
			if (ajaxLoader.size() > 0) {
				var triggerCls = "";
				var visibleDialog = $(".dialog:visible");
				if (visibleDialog.size() > 0) {
					triggerCls = ui.clazz.stackLvl1;
					if (visibleDialog.hasClass(ui.clazz.stackLvl1)) {
						triggerCls = ui.clazz.stackLvl2;
					}	
				}
				var trigger = $("<button>", { "class": triggerCls })
				ui.widget.showDialog(ajaxLoader, trigger);
			}
		},
		closeAjaxLoader: function() {
			var ajaxLoader = $("#dlg-ajax-loader");
			ui.widget.closeDialog(ajaxLoader);	
		},
		hideAjaxLoader: function() {
			var ajaxLoader = $("#dlg-ajax-loader").hide();	
			var overlay = $("#dialog-overlay");
			overlay.removeClass(ui.clazz.stackLvl1);
			overlay.removeClass(ui.clazz.stackLvl2);	
		}, 
		showOverlay: function() {
			var overlay = $("#dialog-overlay");
			overlay.show();
		},
		hideOverlay: function() {
			var overlay = $("#dialog-overlay");
			overlay.hide();
		}
	},
	element : {
		setupSharpLinks: function() {
			$("a[href^=#]").click(function() {
				return false;
			});
		}
	}
};