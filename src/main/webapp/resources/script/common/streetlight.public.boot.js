﻿$(document).ready(function() {	
	/*ui.element.setupSharpLinks();
	ui.widget.setupHeaderContextMenus();
	ui.widget.setupSideNavigation();
	ui.widget.setupContentBoxes();
	ui.widget.setupDatePicker();
	
	
	$(window).load(function() {				
		$("#wrapper").fadeIn(settings.effect.speed, function() {			
			ui.layout.resizeMainContent();
			ui.layout.resizeSideContent();
			$(this).trigger('afterShow');					
		});
	});*/
	
	ui.widget.setupDialogs();
	
	ui.layout.resizeMainContent();
	ui.layout.resizeSideContent();
	ui.layout.setFilterAnimation();
	
	$(window).resize(function() {
		ui.layout.resizeMainContent();
		ui.layout.resizeSideContent();
	});
});