$(document).ready(function(e){
	$("#selectSearchType").change(function(){
		var alarmType =$("#selectSearchType option:selected").val();
		if (alarmType == 1) {
			$("#searchKey").show();
			$("#selectZone").hide();
		} else {
			$("#searchKey").hide();
			$("#selectZone").show();
		}
	});
});