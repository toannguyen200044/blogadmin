angular.module("myApp").controller("UserController", ['$scope', 'userService','$interval', '$window', function($scope,userService, $interval, $window) {
	
	  $scope.data =[];
	  $scope.total_items = 0;
	  $scope.toDelete=[];
	  $scope.checkstatus;
	  $scope.name = null;
	  $scope.indexRequest = 0;
	  $scope.returnRecords = 50;
	  $scope.userName = null;
	  $scope.userId= null;
	  

	$scope.init = function (){
		$.blockUI({ 
	           message: '<div class="fa-5x"><i class="fas fa-spinner fa-pulse"></i></div>',
	    });
		userService.getUser($scope.indexRequest,$scope.returnRecords).then(function(result){
			if(result.data != null){
				$scope.data = result.data.listResult;
				$scope.total_items = result.data.totalRecords;
				$('.pagination').show();
				$('#UserList').find('#notDataFoundLabel').remove();
			}else{
				$scope.data = null;
			}
			
			if($scope.data == null || $scope.data.length == 0) {
				$('.pagination').hide();
				$('#UserList').find('#notDataFoundLabel').remove();
				$('#UserList').append("<label id='notDataFoundLabel' style='padding-left: 30px;'>" + MESSAGE_CONSTANT.noData + "</label>");
			} 
			$scope.checkstatus = result.status;
			$.unblockUI();
	    });
		
		$scope.selectedAll = false;
		if($scope.data != null){
			for (var i = 0; i < $scope.data.length; i++) {
				$scope.toDelete[$scope.data[i].id] = $scope.selectedAll;
			}
		}
	};
	$scope.init();
	
	$('#selectSearchType').on('change', function() {
		switch ($('#selectSearchType option:selected').val()) {
		  case 'username':
		  case 'name':
			$('#selectUserProfileSearch').hide();
			break;
		}
	});
	
	$scope.search = function(){
		$scope.txtUserName=null;
		$scope.txtName=null;
		switch ($('#selectSearchType option:selected').val()) {
		  case 'username':
			  $scope.txtUserName = $('.input-group #searchKey').val();
			break;
		  case 'name':
			  $scope.txtName = $('.input-group #searchKey').val();
		}
		userService.getUser($scope.indexRequest,$scope.returnRecords,$scope.txtUserName,$scope.txtName).then(function(result){
			if(result.data != null){
				$scope.data = result.data.listResult;
				$scope.total_items = result.data.totalRecords;
				$('.pagination').show();
				$('#UserList').find('#notDataFoundLabel').remove();
			}else{
				$scope.data = null;
			}
			
			if($scope.data == null || $scope.data.length == 0) {
				$('.pagination').hide();
				$('#UserList').find('#notDataFoundLabel').remove();
				$('#UserList').append("<label id='notDataFoundLabel' style='padding-left: 30px;'>" + MESSAGE_CONSTANT.noData + "</label>");
			} 
			$scope.checkstatus = result.status;
			$.unblockUI();
	    });
	}
	
	$scope.selectAll = function () {
		if($scope.data != null){
			for (var i = 0; i < $scope.data.length; i++) {
				$scope.toDelete[$scope.data[i].id] = $scope.selectedAll;
			}
		}
	};
		
	$scope.checkIfSelectAll = function () {
		if($scope.data != null){
			$scope.selectedAll = $scope.data.every(function(value) {
				return value.toDelete == true
			})
		}
	};	
	
	/**LOAD DATA USER**/
	$scope.userData;
	$scope.loadEditUser = function(value) {
  	  userService.loadEditUser(value).then(function(result){
  		$scope.userData = result.data;
  	  });  
    };
    
    /**UPDATE USER**/
    var url = window.location.href;
	var parameters = url.split("=");
	var userID = parameters[1];
		
	$scope.updateUser = function() {
		$scope.id = $("#txtId").val();
		$scope.name = $("#txtName").val();
		$scope.password = $("#txtpassword").val();
		$scope.address = $("#txtaddress").val();
		$scope.phone = $("#txtphone").val();
		$scope.email = $("#txtemail").val();
		$scope.username = $("#txtusername").val();
		if($scope.txtusername == "") {
			swal(MESSAGE_CONSTANT.warningUserName, null, "warning");
			return;
		}
			userService.updateUser($scope.id, $scope.username, $scope.name, $scope.password, $scope.email, $scope.address,$scope.phone).then(function(response){
				if(response != null && response.validData){
					swal({
						  title: "",
//						  text: MESSAGE_CONSTANT.updateMlcSuccess,
						  type: "Update Success",
						  showCancelButton: false,
						  confirmButtonColor: "#DD6B55",
						  closeOnConfirm: true,
						  html: true
						}, function(){
							$window.location.href = '/final-excercise/user';
						});
		    	} else {
		    		swal('',response.listErrorMessage[0], 'error');
		    	}
		    }).catch(function(error){
		    	console.log(error);
		    });			
	};
	
	$scope.userAddForm = function() {
		$scope.txtUserName = document.getElementById('addUserName').value;
		if($scope.txtUserName.trim() == "") {
			swal(MESSAGE_CONSTANT.warningUserName, null, "warning");
			return;
		}
		
		$scope.txtPassword = document.getElementById('addPassword').value;
		if($scope.txtPassword.trim() == "") {
			swal(MESSAGE_CONSTANT.warningUserName, null, "warning");
			return;
		}
		
		$scope.txtEmail = document.getElementById('addEmail').value;
		if($scope.txtEmail.trim() == "") {
			swal(MESSAGE_CONSTANT.warningUserName, null, "warning");
			return;
		}
		
		userService.addUser($scope.txtUserName,$scope.txtEmail,$scope.txtPassword).then(function(result) {
			if (result != null) {
				if (result.validData) {
					$window.location.href = '/final-excercise/user';
				}
				if(result.listErrorMessage[0] != null) {
					swal('', result.listErrorMessage[0], 'error');
				}
				if(result.messeageFail != null) {
					swal('', result.messeageFail, 'error');
				}
			}
		});
	};
	
	$scope.deleteOneUser = function(id) {		
		swal({
			  title: "Are you sure?",
//			  text: MESSAGE_CONSTANT.confirmDeleteOneUser + id +" ?",
			  type: "warning",
			  allowOutsideClick: true,
			  confirmButtonClass: "btn-inverse",
			  confirmButtonText: "Yes",
			  showCancelButton: true,			  
			  cancelButtonText: "No",
			  cancelButtonClass: "btn-default",
			  closeOnConfirm:false
		},function(is_confirmed){
			if(is_confirmed){
				userService.deleteUser(id).then(function(response){
					if(response.validData == true) {
						$window.location.href = '/final-excercise/user';
					} else {
                        if(response.messeageFail != null){
                            swal(response.messeageFail,null, "error");
                        }
        				if(result.listErrorMessage[0] != null){
        					swal('',result.listErrorMessage[0],'error');
        				}
					}
					$scope.init();
			    });
			}
		});	
	}
	
	$scope.deleteAllUser = function(){
		var listuserid="";
		for ( id in $scope.toDelete ){
			if ($scope.toDelete[id]) {
				listuserid = listuserid+ id+ ",";
			}
		}
		var userids="";
		userids = listuserid.substring(0, listuserid.length - 1);
		if(userids == ""){
			swal(MESSAGE_CONSTANT.warningChooseMultiItems, null, "warning");
			return;
		}
		swal({
			 title: "",
//			  text: MESSAGE_CONSTANT.confirmDeleteMultiUser,
			  type: "warning",
			  allowOutsideClick: true,
			  confirmButtonClass: "btn-inverse",
			  confirmButtonText: "Yes",
			  showCancelButton: true,			  
			  cancelButtonText: "No",
			  cancelButtonClass: "btn-default",
			  closeOnConfirm:false
		},function(is_confirmed){
			if(is_confirmed == true){
					userService.deleteUser(userids).then(function(response) {
                    	if(response.result == null) {
        					if(response.validData == true) {
        					$window.location.href = '/final-excercise/user';
        					}
                    	}
                    	$scope.init();
                    });
			}
		});
	}

}]);