angular.module('myApp').factory("userService", userService);

function userService($http, $window) {

	function getUser(indexRequest, returnRecords) {
		var parameter = {
			indexRequest : indexRequest,
			returnRecords : returnRecords,
		};

		var request = $http.post('user/search', parameter);

		return request.then(function(response) {
			return response;
		},
				function(err) {
					console.log(err);
					if (err.status != null
							&& err.status == COMMON_CONSTANT.httpTimeout) {
						$window.location.href = '/final-excercise/login';
						return;
					}
				});
	}

	function updateUser(id, username, name, password, email, address, phone) {
		var parameter = {
			userID : id,
			username : username,
			name : name,
			password : password,
			email : email,
			phone : phone,
			address : address
		};
		var request = $http.post('user/update', parameter);
		return request.then(function(response) {
			return response.data;
		},
				function(err) {
					console.log(err);
					if (err.status != null
							&& err.status == COMMON_CONSTANT.httpTimeout) {
						$window.location.href = '/final-excercise/login';
						return;
					}
				});
	}

	function loadEditUser(value) {
		var request = $http.get('user/update' + '?userId=' + value);

		return request.then(function(response) {
			return response;
		},
				function(err) {
					console.log(err);
					if (err.status != null
							&& err.status == COMMON_CONSTANT.httpTimeout) {
						$window.location.href = '/final-excercise/login';
						return;
					}
				});
	}

	function addUser(txtUserName,txtEmail,txtPassword) {
		var parameter = {
			username : txtUserName,
			password : txtPassword,
			email : txtEmail
		};

		var request = $http.post('user/Add', parameter);
		return request.then(function(response) {
			return response.data;
		},
				function(err) {
					console.log(err);
					if (err.status != null
							&& err.status == COMMON_CONSTANT.httpTimeout) {
						$window.location.href = '/final-excercise/login';
						return;
					}
				});
	}

	function deleteUser(id) {
		var parameter = {
			userIds : id,
		};

//		var request = $http.post('user/delete', parameter);
		
		var request = $http({
			method: 'POST',
			url: 'user/delete',
			data: parameter,
			headers: {
			    'Content-Type': 'application/json' // Note the appropriate header
			  }
		})
		
		
		return request.then(function(response) {
			return response.data;
		},
				function(err) {
					console.log(err);
					if (err.status != null
							&& err.status == COMMON_CONSTANT.httpTimeout) {
						$window.location.href = '/final-excercise/login';
						return;
					}
				});
	}

	return {
		getUser : getUser,
		updateUser : updateUser,
		loadEditUser : loadEditUser,
		addUser : addUser,
		deleteUser : deleteUser
	};

}
