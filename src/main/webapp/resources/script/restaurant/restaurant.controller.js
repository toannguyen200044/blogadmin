angular.module("myApp").controller("RestaurantController", ['$scope', 'restaurantService','$interval', '$window', function($scope,restaurantService, $interval, $window) {
	
	  $scope.data =[];
	  $scope.total_items = 0;
	  $scope.toDelete=[];
	  $scope.checkstatus;
	  $scope.indexRequest = 0;
	  $scope.returnRecords = 50;
	  var markerRestaurant = null;
	  var isCreating = false;
	$scope.init = function (){
		$.blockUI({ 
	           message: '<div class="fa-5x"><i class="fas fa-spinner fa-pulse"></i></div>',
	    });
		restaurantService.getRestaurant($scope.indexRequest,$scope.returnRecords).then(function(result){
			if(result.data != null){
				$scope.data = result.data.listResult;
				$scope.total_items = result.data.totalRecords;
				$('.pagination').show();
				$('#RestaurantList').find('#notDataFoundLabel').remove();
			}else{
				$scope.data = null;
			}
			
			if($scope.data == null || $scope.data.length == 0) {
				$('.pagination').hide();
				$('#RestaurantList').find('#notDataFoundLabel').remove();
				$('#RestaurantList').append("<label id='notDataFoundLabel' style='padding-left: 30px;'>" + "No data" + "</label>");
			} 
			$scope.checkstatus = result.status;
			$.unblockUI();
	    });
		
		$scope.selectedAll = false;
		if($scope.data != null){
			for (var i = 0; i < $scope.data.length; i++) {
				$scope.toDelete[$scope.data[i].id] = $scope.selectedAll;
			}
		}
	};
	$scope.init();
	
	
	
	$scope.selectAll = function () {
		if($scope.data != null){
			for (var i = 0; i < $scope.data.length; i++) {
				$scope.toDelete[$scope.data[i].id] = $scope.selectedAll;
			}
		}
	};
		
	$scope.checkIfSelectAll = function () {
		if($scope.data != null){
			$scope.selectedAll = $scope.data.every(function(value) {
				return value.toDelete == true
			})
		}
	};	
    
	$scope.map = function() {
	    var styles = [
	      {
	        stylers: [
	          { lightness: -25 },
	          { saturation: -50 },
	          { gamma: 0.75 }
	        ]
	      },{
	        featureType: "road",
	        elementType: "geometry",
	        stylers: [
	          { lightness: 10 },
	          { visibility: "simplified" }
	        ]
	      },{
	        featureType: "road",
	        elementType: "labels",
	        stylers: [
	          { visibility: "simplified" }
	        ]
	      },{
	        featureType: "landscape.man_made",
	        elementType: "geometry",
	        stylers: [
	          { visibility: "simplified" }
	        ]
	      },{
	        featureType: "poi",
	        elementType: "labels",
	        stylers: [
	          { visibility: "off" }
	        ]
	      }
	    ];
	    var styledMap = new google.maps.StyledMapType(styles, {name: "Dark Map"});
	
	    var mapProp = {
	              center:new google.maps.LatLng(10.762622,106.660172),
	              zoom:18,
	              scaleControl: false,
	              zoomControl: true,
	              streetViewControl: false,
	              rotateControl: false,
	              panControl: false,
	              gestureHandling: 'cooperative',
	              mapTypeControl: true,
	              mapTypeControlOptions: {
	                      position: google.maps.ControlPosition.TOP_LEFT,
	                      style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
	                      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
	                  },
	
	              mapTypeId:google.maps.MapTypeId.ROADMAP
	    };
	    var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
	
	    // Associate the styled map with the
			// MapTypeId
			// and set it to display.
	    map.mapTypes.set('map_style', styledMap);
	    map.setMapTypeId('map_style');
	    google.maps.event.addListener(map, "click", function(event) {
			if(isCreating) return;
			
			isCreating = true;
			var lat = event.latLng.lat();
		    var lng = event.latLng.lng();
		    var markerImage = {
					anchor: new google.maps.Point(14, 46),
					url:'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'
		    };

			$('#addLat').val(lat).trigger('input');
			$('#addLong').val(lng).trigger('input');
		    
			markerRestaurant = new google.maps.Marker ({
				position: new google.maps.LatLng(lat, lng), 
				draggable: true,
				map: map,
				title: 'Restaurant', 
				icon: markerImage
			});
			
			google.maps.event.addListener(markerRestaurant, "drag", function(event){
		    	lat =  event.latLng.lat();
		    	lng =  event.latLng.lng();

				$('#addLat').val(lat).trigger('input');
				$('#addLong').val(lng).trigger('input');
		    });
			
		});
	
	  }
	$scope.map();
	$scope.search = function(str) {
	        var d = $q.defer();
	        this.places.textSearch({query: str}, function(results, status) {
	            if (status == 'OK') {
	                d.resolve(results[0]);
	            }
	            else d.reject(status);
	        });
	        return d.promise;
	    }
	    
	    $scope.addMarker = function(res) {
	        if(this.marker) this.marker.setMap(null);
	        this.marker = new google.maps.Marker({
	            map: this.map,
	            position: res.geometry.location,
	            animation: google.maps.Animation.DROP
	        });
	        this.map.setCenter(res.geometry.location);
	    }
	    
	    
	    $scope.place = {};
	    
	    $scope.searchMap = function() {
	        $scope.apiError = false;
	        $scope.search($scope.searchPlace)
	        .then(
	            function(res) { // success
	            	$scope.addMarker(res);
	                $scope.place.name = res.name;
	                $scope.place.lat = res.geometry.location.lat();
	                $scope.place.lng = res.geometry.location.lng();
	            },
	            function(status) { // error
	                $scope.apiError = true;
	                $scope.apiStatus = status;
	            }
	        );
	    }
	    
	    $scope.send = function() {
	        alert($scope.place.name + ' : ' + $scope.place.lat + ', ' + $scope.place.lng);    
	    }
	    
    
	$scope.restaurantAddForm = function() {
		$scope.txtRestaurantName = document.getElementById('addRestaurantName').value;
		if($scope.txtRestaurantName.trim() == "") {
			swal(MESSAGE_CONSTANT.warningFoodName, null, "warning");
			return;
		}
		
		$scope.txtRestaurantImage = document.getElementById('addRestaurantImage').value;
		if($scope.txtRestaurantImage.trim() == "") {
			swal(MESSAGE_CONSTANT.warningRestaurantImage, null, "warning");
			return;
		}
		
		$scope.txtLat = document.getElementById('addLat').value;
		if($scope.txtLat.trim() == "") {
			swal(MESSAGE_CONSTANT.warningLat, null, "warning");
			return;
		}
		$scope.txtLong = document.getElementById('addLong').value;
		if($scope.txtLong.trim() == "") {
			swal(MESSAGE_CONSTANT.warningLong, null, "warning");
			return;
		}
		
		$scope.txtRegistrationDate = document.getElementById('addRegistrationDate').value;
		$scope.txtDayEnd = document.getElementById('addDayEnd').value;
		$scope.txtNumber = document.getElementById('addNumber').value;
		if($scope.txtNumber.trim() == "") {
			swal(MESSAGE_CONSTANT.warningNumber, null, "warning");
			return;
		}
		$scope.txtOpenTime = document.getElementById('addOpenTime').value;
		$scope.txtCloseTime = document.getElementById('addCloseTime').value;
		
		$scope.txtAddress = document.getElementById('addAddress').value;
		if($scope.txtAddress.trim() == "") {
			swal(MESSAGE_CONSTANT.warningNumber, null, "warning");
			return;
		}
		restaurantService.addRestaurant($scope.txtRestaurantName,$scope.txtRestaurantImage,$scope.txtAddress,$scope.txtLat,$scope.txtLong,$scope.txtRegistrationDate,$scope.txtDayEnd,$scope.txtNumber,$scope.txtOpenTime,$scope.txtCloseTime,$scope.txtDistrict).then(function(result) {
				if (result != null) {
					if (result.validData) {
						$window.location.href = '/final-excercise/restaurant';
					}
					if(result.listErrorMessage[0] != null) {
						swal('', result.listErrorMessage[0], 'error');
					}
					if(result.messeageFail != null) {
						swal('', result.messeageFail, 'error');
					}
				}
		});
	};
	
	$scope.deleteOneRestaurant = function(id) {		
		swal({
			  title: "Are you sure?",
//			  text: MESSAGE_CONSTANT.confirmDeleteOneUser + id +" ?",
			  type: "warning",
			  allowOutsideClick: true,
			  confirmButtonClass: "btn-inverse",
			  confirmButtonText: "Yes",
			  showCancelButton: true,			  
			  cancelButtonText: "No",
			  cancelButtonClass: "btn-default",
			  closeOnConfirm:false
		},function(is_confirmed){
			if(is_confirmed){
				restaurantService.deleteRestaurant(id).then(function(response){
					if(response.validData == true) {
						$window.location.href = '/final-excercise/restaurant';
					} else {
                        if(response.messeageFail != null){
                            swal(response.messeageFail,null, "error");
                        }
        				if(result.listErrorMessage[0] != null){
        					swal('',result.listErrorMessage[0],'error');
        				}
					}
					$scope.init();
			    });
			}
		});	
	}
	
	$scope.deleteAllRestaurant = function(){
		var listrestaurantid="";
		for ( id in $scope.toDelete ){
			if ($scope.toDelete[id]) {
				listrestaurantid = listrestaurantid + id + ",";
			}
		}
		var restaurantids="";
		restaurantids = listrestaurantid.substring(0, listrestaurantid.length - 1);
		if(restaurantids == ""){
			swal(MESSAGE_CONSTANT.warningChooseMultiItems, null, "warning");
			return;
		}
		swal({
				title: "Are you sure ?",
			  type: "warning",
			  allowOutsideClick: true,
			  confirmButtonClass: "btn-inverse",
			  confirmButtonText: "Yes",
			  showCancelButton: true,			  
			  cancelButtonText: "No",
			  cancelButtonClass: "btn-default",
			  closeOnConfirm:false
		},function(is_confirmed){
			if(is_confirmed == true){
				restaurantService.deleteRestaurant(restaurantids).then(function(response) {
                    	if(response.result == null) {
        					if(response.validData == true) {
        						$window.location.href = '/final-excercise/restaurant';
        					}
                    	}
                    	$scope.init();
                    });
			}
		});
	}

}]);