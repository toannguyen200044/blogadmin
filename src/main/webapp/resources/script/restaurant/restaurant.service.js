angular.module('myApp').factory("restaurantService", restaurantService);

function restaurantService($http, $window) {

	function getRestaurant(indexRequest, returnRecords) {
		var parameter = {
			indexRequest : indexRequest,
			returnRecords : returnRecords
		};

		var request = $http.post('restaurant/search', parameter);

		return request.then(function(response) {
			return response;
		},
				function(err) {
					console.log(err);
					if (err.status != null
							&& err.status == COMMON_CONSTANT.httpTimeout) {
						$window.location.href = '/final-excercise/login';
						return;
					}
				});
	}
	

	function addRestaurant(txtRestaurantName,txtRestaurantImage,txtAddress,txtLat,txtLong,txtRegistrationDate,txtDayEnd,txtNumber,txtOpenTime,txtCloseTime) {
		var parameter = {
			restaurant_name : txtRestaurantName,
			restaurantImage : txtRestaurantImage,
			restaurant_address : txtAddress,
			number : txtNumber,
			latitude : txtLat,
			longtitude : txtLong,
			registration_date : txtRegistrationDate,
			end_date : txtDayEnd,
			open_time : txtOpenTime,
			close_time : txtCloseTime,
		};
		var request = $http.post('restaurant/Add', parameter);
		return request.then(function(response) {
			return response.data;
		},
				function(err) {
					console.log(err);
					if (err.status != null
							&& err.status == COMMON_CONSTANT.httpTimeout) {
						$window.location.href = '/final-excercise/login';
						return;
					}
				});
	}

	function deleteRestaurant(id) {
		var parameter = {
			restaurantIds : id
		};

		// var request = $http.post('user/delete', parameter);

		var request = $http({
			method : 'POST',
			url : 'restaurant/delete',
			data : parameter,
			headers : {
				'Content-Type' : 'application/json' // Note the appropriate
													// header
			}
		})

		return request.then(function(response) {
			return response.data;
		},
				function(err) {
					console.log(err);
					if (err.status != null
							&& err.status == COMMON_CONSTANT.httpTimeout) {
						$window.location.href = '/final-excercise/login';
						return;
					}
				});
	}

	return {
		getRestaurant : getRestaurant,
		addRestaurant : addRestaurant,
		deleteRestaurant : deleteRestaurant,
	};

}
