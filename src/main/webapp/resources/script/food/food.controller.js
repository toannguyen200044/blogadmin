angular.module("myApp").controller("FoodController", ['$scope', 'foodService','$interval', '$window', function($scope,foodService, $interval, $window) {
	
	  $scope.data =[];
	  $scope.total_items = 0;
	  $scope.toDelete=[];
	  $scope.checkstatus;
	  $scope.name = null;
	  $scope.indexRequest = 0;
	  $scope.returnRecords = 50;
	  $scope.userName = null;
	  $scope.userId= null;
	  

	$scope.init = function (){
		$.blockUI({ 
	           message: '<div class="fa-5x"><i class="fas fa-spinner fa-pulse"></i></div>',
	    });
		foodService.getFood($scope.indexRequest,$scope.returnRecords).then(function(result){
			if(result.data != null){
				$scope.data = result.data.listResult;
				$scope.total_items = result.data.totalRecords;
				$('.pagination').show();
				$('#FoodList').find('#notDataFoundLabel').remove();
			}else{
				$scope.data = null;
			}
			
			if($scope.data == null || $scope.data.length == 0) {
				$('.pagination').hide();
				$('#FoodList').find('#notDataFoundLabel').remove();
				$('#FoodList').append("<label id='notDataFoundLabel' style='padding-left: 30px;'>" + "No data" + "</label>");
			} 
			$scope.checkstatus = result.status;
			$.unblockUI();
	    });
		
		$scope.selectedAll = false;
		if($scope.data != null){
			for (var i = 0; i < $scope.data.length; i++) {
				$scope.toDelete[$scope.data[i].id] = $scope.selectedAll;
			}
		}
	};
	$scope.init();
	
	
	
	$scope.selectAll = function () {
		if($scope.data != null){
			for (var i = 0; i < $scope.data.length; i++) {
				$scope.toDelete[$scope.data[i].id] = $scope.selectedAll;
			}
		}
	};
		
	$scope.checkIfSelectAll = function () {
		if($scope.data != null){
			$scope.selectedAll = $scope.data.every(function(value) {
				return value.toDelete == true
			})
		}
	};	
	
	$scope.loadEditFoodData = function(value) {
		foodService.loadEditFood(value).then(function(result){
	  		$scope.foodData = result.data.foodDto;
	  		$("#addRestaurant").val($scope.result); 
	  	  }); 
	}
	
	/**UPDATE USER**/
    var url = window.location.href;
	var parameters = url.split("=");
	var id = parameters[1];
		
	$scope.updateFood = function() {
		$scope.id = $("#txtId").val();
		$scope.nameFood = $("#txtfoodName").val();
		$scope.category = $("#txtcategory").val();
		$scope.price = $("#txtprice").val();
		$scope.restaurantId = $("#txtRestaurant").val();
		$scope.image = $("#txtImage").val();
			foodService.updateFood($scope.id, $scope.nameFood, $scope.category, $scope.price, $scope.restaurantId, $scope.image).then(function(response){
					if (response.validData) {
						swal({
							  title: "",
//							  text: MESSAGE_CONSTANT.updateMlcSuccess,
							  type: "success",
							  showCancelButton: false,
							  confirmButtonColor: "#DD6B55",
							  closeOnConfirm: false,
							  html: true
							}, function(){
								$window.location.href = '/final-excercise/food';
							});
					} else {
						swal('', result.listErrorMessage[0], 'error');
					}
					
		    }).catch(function(error){
		    	console.log(error);
		    });			
	};
	
	$scope.foodAddForm = function() {
		$scope.txtFoodName = document.getElementById('addFoodName').value;
		if($scope.txtFoodName.trim() == "") {
			swal(MESSAGE_CONSTANT.warningFoodName, null, "warning");
			return;
		}
		
		$scope.txtPrice = document.getElementById('addPrice').value;
		if($scope.txtPrice.trim() == "") {
			swal(MESSAGE_CONSTANT.warningPrice, null, "warning");
			return;
		}
		
		$scope.txtRestaurant = document.getElementById('addRestaurant').value;
		if($scope.txtRestaurant.trim() == "") {
			swal(MESSAGE_CONSTANT.warningRestaurant, null, "warning");
			return;
		}
		
		$scope.txtCategory = document.getElementById('addCategory').value;
		if($scope.txtCategory.trim() == "") {
			swal(MESSAGE_CONSTANT.warningCategory, null, "warning");
			return;
		}
		
		$scope.txtImage = document.getElementById('addImage').value;
		if($scope.txtImage.trim() == "") {
			swal(MESSAGE_CONSTANT.warningCategory, null, "warning");
			return;
		}
		
		foodService.addFood($scope.txtFoodName,$scope.txtPrice,$scope.txtRestaurant,$scope.txtCategory,$scope.txtImage).then(function(result) {
			if (result != null) {
				if (result.validData) {
					$window.location.href = '/final-excercise/food';
				}
				if(result.listErrorMessage[0] != null) {
					swal('', result.listErrorMessage[0], 'error');
				}
				if(result.messeageFail != null) {
					swal('', result.messeageFail, 'error');
				}
			}
		});
	};
	
	$scope.deleteOneFood = function(id) {		
		swal({
			  title: "Are you sure?",
//			  text: MESSAGE_CONSTANT.confirmDeleteOneUser + id +" ?",
			  type: "warning",
			  allowOutsideClick: true,
			  confirmButtonClass: "btn-inverse",
			  confirmButtonText: "Yes",
			  showCancelButton: true,			  
			  cancelButtonText: "No",
			  cancelButtonClass: "btn-default",
			  closeOnConfirm:false
		},function(is_confirmed){
			if(is_confirmed){
				foodService.deleteFood(id).then(function(response){
					if(response.validData == true) {
						swal({
							  title: "Are you sure ?",
//							  text: MESSAGE_CONSTANT.updateMlcSuccess,
							  type: "success",
							  showCancelButton: false,
							  confirmButtonColor: "#DD6B55",
							  closeOnConfirm: true,
							  html: false
							}, function(){
								$window.location.href = '/final-excercise/food';
							});
					} else {
                        if(response.messeageFail != null){
                            swal(response.messeageFail,null, "error");
                        }
        				if(result.listErrorMessage[0] != null){
        					swal('',result.listErrorMessage[0],'error');
        				}
					}
					$scope.init();
			    });
			}
		});	
	}
	
	$scope.deleteAllFood = function(){
		var listfoodid="";
		for ( id in $scope.toDelete ){
			if ($scope.toDelete[id]) {
				listfoodid = listfoodid + id+ ",";
			}
		}
		var foodids="";
		foodids = listfoodid.substring(0, listfoodid.length - 1);
		if(foodids == ""){
			swal(MESSAGE_CONSTANT.warningChooseMultiItems, null, "warning");
			return;
		}
		swal({
			 title: "",
//			  text: MESSAGE_CONSTANT.confirmDeleteMultiUser,
			  type: "warning",
			  allowOutsideClick: true,
			  confirmButtonClass: "btn-inverse",
			  confirmButtonText: "Yes",
			  showCancelButton: true,			  
			  cancelButtonText: "No",
			  cancelButtonClass: "btn-default",
			  closeOnConfirm:false
		},function(is_confirmed){
			if(is_confirmed == true){
				foodService.deleteFood(foodids).then(function(response) {
                    	if(response.result == null) {
        					if(response.validData == true) {
        						swal({
      							  title: "Are you sure ?",
//      							  text: MESSAGE_CONSTANT.updateMlcSuccess,
      							  type: "success",
      							  showCancelButton: false,
      							  confirmButtonColor: "#DD6B55",
      							  closeOnConfirm: true,
      							  html: false
      							}, function(){
      								$window.location.href = '/final-excercise/food';
      							});
        					} else {
                                if(response.messeageFail != null){
                                    swal(response.messeageFail, null, "error");
                                }
                				if(response.listErrorMessage[0] != null){
                					swal('',response.listErrorMessage[0],'error');
                				}
        					}
                    	} else {
                        	if(response.result == true) {
                        		swal('',MESSAGE_CONSTANT.deleteMLCSuccess, "success");
                        	} else {
                                if(response.messeageFail != null){
                                    swal(response.messeageFail, null, "error");
                                }
                                if(response.messeageResult != null){
                                    swal(response.messeageResult, null, "warning");
                                }
                        	}
                    	}
                    	$scope.init();
                    });
			}
		});
	}

}]);