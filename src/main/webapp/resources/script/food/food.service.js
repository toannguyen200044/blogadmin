angular.module('myApp').factory("foodService", foodService);

function foodService($http, $window) {

	function getFood(indexRequest, returnRecords) {
		var parameter = {
			indexRequest : indexRequest,
			returnRecords : returnRecords
		};

		var request = $http.post('food/search', parameter);

		return request.then(function(response) {
			return response;
		},
				function(err) {
					console.log(err);
					if (err.status != null
							&& err.status == COMMON_CONSTANT.httpTimeout) {
						$window.location.href = '/final-excercise/login';
						return;
					}
				});
	}
	
	function loadEditFood(value) {
		var request = $http.get('food/update' + '?foodId=' + value);

		return request.then(function(response) {
			return response;
		},
				function(err) {
					console.log(err);
					if (err.status != null
							&& err.status == COMMON_CONSTANT.httpTimeout) {
						$window.location.href = '/final-excercise/login';
						return;
					}
				});
	}

	function addFood(txtFoodName, txtPrice,txtRestaurant,txtCategory,txtImage) {
		var parameter = {
			name_food : txtFoodName,
			price : txtPrice,
			restaurantId : txtRestaurant,
			category : txtCategory,
			image : txtImage
		};

		var request = $http.post('food/Add', parameter);
		return request.then(function(response) {
			return response.data;
		},
				function(err) {
					console.log(err);
					if (err.status != null
							&& err.status == COMMON_CONSTANT.httpTimeout) {
						$window.location.href = '/final-excercise/login';
						return;
					}
				});
	}

	function deleteFood(id) {
		var parameter = {
			foodIds : id,
		};

		// var request = $http.post('user/delete', parameter);

		var request = $http({
			method : 'POST',
			url : 'food/delete',
			data : parameter,
			headers : {
				'Content-Type' : 'application/json' // Note the appropriate
													// header
			}
		})

		return request.then(function(response) {
			return response.data;
		},
				function(err) {
					console.log(err);
					if (err.status != null
							&& err.status == COMMON_CONSTANT.httpTimeout) {
						$window.location.href = '/final-excercise/login';
						return;
					}
				});
	}
	
	function updateFood(id, nameFood, category, price, restaurantId, image) {
		var parameter = {
			id : id,
			name_food : nameFood,
			category : category,
			price : price,
			restaurantId : restaurantId,
			image : image
		};
		var request = $http.post('food/update', parameter);
		return request.then(function(response) {
			return response.data;
		},
				function(err) {
					console.log(err);
					if (err.status != null
							&& err.status == COMMON_CONSTANT.httpTimeout) {
						$window.location.href = '/final-excercise/login';
						return;
					}
				});
	}

	return {
		getFood : getFood,
		addFood : addFood,
		deleteFood : deleteFood,
		loadEditFood:loadEditFood,
		updateFood:updateFood
	};

}
